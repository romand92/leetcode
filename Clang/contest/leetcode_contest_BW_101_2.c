/*

*/

#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#define SIZEOFTESTARRAY 4

int maximumCostSubstring(char * s, char * chars, int* vals, int valsSize){
    int l=0,r=0;
    int max_cost = 0;
    
    int hash_prices[26];
    for(int i = 0; i< 26;i++){
        hash_prices[i] = i+1;
    }

    // int all_positive = 1;
    // int all_negative = 1;
    for(int i = 0;chars[i];i++){
        // if(all_positive && vals[i]<0) 
        //     all_positive =0;
        // if(all_negative && vals[i]>0) 
        //     all_negative =0;
        hash_prices[chars[i] - 'a'] = vals[i];
    }
    // printf("allPos:%d allNeg:%d\n",all_positive,all_negative);
    // for(int i = 0; i< 26;i++){
    //     printf("%d ",hash_prices[i]);
    // }

    int max_so_far = -2147483647, max_ending_here = 0;

    int size = strlen(s);
    for (int i = 0; i < size; i++) {
        max_ending_here = max_ending_here + hash_prices[s[i]-'a'];
        if (max_so_far < max_ending_here)
            max_so_far = max_ending_here;
 
        if (max_ending_here < 0)
            max_ending_here = 0;
    }
    return max_so_far;


}




int main() {
    char *s = "abc", *chars = "abc";
    int vals[] = {-1,-2,-1};
    int res = maximumCostSubstring(s,chars,vals,3);
    printf("\n%d\n",res);
}
