/*

*/

#include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 4

int minNumber(int* nums1, int nums1Size, int* nums2, int nums2Size){
    
    int hash_t[10] = {0};

    int min1 = 10;
    for(int i = 0; i<nums1Size; i++ ){
        hash_t[nums1[i]]++;
        if (nums1[i] < min1)
            min1 = nums1[i];
    }
    
    int min2 = 10;
    for(int i =0; i<nums2Size; i++ ){
        hash_t[nums2[i]]++;
        if (nums2[i] < min2)
            min2 = nums2[i];
    }

    for(int i = 0; i < 10;i++)
        if (hash_t[i]>1)
            return i;
    return (min1>min2) ? min2*10+min1 : min1*10+min2;

}


int main() {
    int nums1[] = {1,3,2,5,9}, nums2[] = {9};

    printf("\n%d\n",minNumber(nums1,5,nums2,1));
}
