// You are given an integer array nums where the largest integer is unique.
// Determine whether the largest element in the array is at least twice as much
// as every other number in the array. If it is, return the index of the largest
// element, or return -1 otherwise.
#include <stdio.h>

int calculateTrips(int* time, int timeSize, long long time_spent, int totalTrips) {
    long long trips = 0;
    for (int i = 0; i < timeSize; i++) {
        trips += time_spent / time[i];
    }
    // printf(" trips:%lld\n",trips);
    return ((trips >= totalTrips) ? 1 : 0);
}

long long minimumTime(int* time, int timeSize, int totalTrips) {
    long long max_time = 1e12;
    long long min_time = 1;
    long long mid_time;
    int trips = 0;

    while (min_time != max_time) {
        mid_time = (min_time + max_time) / 2;
        trips = calculateTrips(time, timeSize, mid_time, totalTrips);
        // printf("%d\n",trips);
        if (trips)
            max_time = mid_time;
        else
            min_time = mid_time + 1;
        // printf("m_t:%lld;",mid_time);
    }
    // printf("tr:%d ",time_spent);
    return min_time;
}

int main() {
    int test_arr[5][4] = {{3, 3, 8}, {1, 2, 3, 4}, {0, 0, 0, 1}, {0, 0, 3, 2}, {1, 0}};
    int test_n[] = {6, 2, 3, 4, 5};
    printf("%ld", minimumTime(test_arr[0], 3, test_n[0]));

    // for (int i = 0; i < 5; i++) {
    //     printf("%d\n", dominantIndex(test[i], (i == 4) ? 2 : 4));
    // }
}