/*
Design a data structure that supports adding new words and finding if a string matches any previously added
string.

Implement the WordDictionary class:

WordDictionary() Initializes the object.
void addWord(word) Adds word to the data structure, it can be matched later.
bool search(word) Returns true if there is any string in the data structure that matches word or false
otherwise. word may contain dots '.' where dots can be matched with any letter.
*/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#define SIZEOFTESTARRAY 6

#define N 26
typedef struct TrieNode TrieNode, WordDictionary;
struct TrieNode {
    char data;
    TrieNode* children[N];
    int is_leaf;
};

TrieNode* init_trienode(char data) {
    // Allocate memory for a TrieNode
    TrieNode* node = (TrieNode*)malloc(sizeof(TrieNode));
    for (int i = 0; i < N; i++) node->children[i] = NULL;
    node->is_leaf = 0;
    node->data = data;
    return node;
}

WordDictionary* wordDictionaryCreate() {
    TrieNode* root = init_trienode('\0');
    return root;
}

void wordDictionaryAddWord(WordDictionary* root, char* word) {
    TrieNode* root_cp = root;
    for (; *word; word++) {
        int child_index = *word - 'a';
        if (!root_cp->children[child_index]) {
            root_cp->children[child_index] = init_trienode(*word);
        }
        root_cp = root_cp->children[child_index];
    }
    root_cp->is_leaf = 1;
}

bool wordDictionarySearch(WordDictionary* root, char* word) {
    bool res = false;
    // Searches for word in the Trie
    TrieNode* root_cp = root;
    bool wild = false;
    for (; *word; word++) {
        if (*word == '.') {
            wild = true;
            for (int i = 0; i < N; i++) {
                if (root_cp->children[i]) res = wordDictionarySearch(root_cp->children[i], (word + 1));
                if (res) break;
            }
            return res;
        } else {
            int position = *word - 'a';
            if (root_cp->children[position] == NULL) {
                return false;
            }
            root_cp = root_cp->children[position];
        }
    }
    if (root_cp != NULL && root_cp->is_leaf == 1) res = true;
    return res;
}

void wordDictionaryFree(WordDictionary* node) {
    for (int i = 0; i < N; i++) {
        if (node->children[i] != NULL) {
            wordDictionaryFree(node->children[i]);
        }
    }
    free(node);
}

/**
 * Your WordDictionary struct will be instantiated and called as such:
 * WordDictionary* obj = wordDictionaryCreate();
 * wordDictionaryAddWord(obj, word);

 * bool param_2 = wordDictionarySearch(obj, word);

 * wordDictionaryFree(obj);
*/

int main() {
    WordDictionary* root = wordDictionaryCreate();
    // TrieNode* root = init_trienode('\0');
    wordDictionaryAddWord(root, "at");
    wordDictionaryAddWord(root, "and");
    wordDictionaryAddWord(root, "an");
    wordDictionaryAddWord(root, "add");
    wordDictionaryAddWord(root, "bat");
    char test_word[SIZEOFTESTARRAY][10] = {"a.d.", ".at", "bat", "hi", "...", "a.d"};
    bool expected[SIZEOFTESTARRAY] = {false, true, true, false, true, true};
    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        bool val = wordDictionarySearch(root, test_word[i]);
        printf("Search for:\"%s\" - %s, expected: %s\n", test_word[i], (val == true) ? "True" : "False",
               (expected[i] == true) ? "True" : "False");
    }
}
