#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef HT_KEY_STR_VAL_INT
#define HT_KEY_STR_VAL_INT

struct HT_item {
    char* word;
    int val;  // aka count
};
typedef struct HT_item HT_item;

typedef struct HT {
    HT_item** words;
    unsigned int size;
    unsigned int count;
} HT;

void free_table(HT* table);
void free_item(HT_item* item);
HT* create_table(int size);
HT_item* create_item(char* key, int value);
unsigned int kinda_hash(const char* word, unsigned int base);
void print_table(HT* table);
HT_item* HT_find(struct HT* ht, char* word);
unsigned int collision_handler(struct HT* ht, char* word);
void HT_add(struct HT* ht, char* word);

#endif