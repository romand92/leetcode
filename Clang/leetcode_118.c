/*
Given an integer numRows, return the first numRows of Pascal's triangle.

In Pascal's triangle, each number is the sum of the two numbers directly above it.
*/

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */

#include <stdio.h>
#include <stdlib.h>

#define SIZEOFTESTARRAY 6

int **generate(int numRows, int *returnSize, int **returnColumnSizes) {
    int **res = malloc(numRows * sizeof(int *) + sizeof(int) * (1 + numRows) * numRows / 2);
    // total amount of memory need is (total elements(int) represents values of
    // triange) + (numRows of pointers(int*) to point array as 2d) total values of
    // triangle equal to sum of sequence from 1 to n where n = numRows. Formula is
    // (a[1] +a[n])*n/2 => (1 + numRows)*numRows/2
    int *ptr = (int *)(res + numRows);  // pointer to point Rows of 2d array
    int *colsize = malloc(sizeof(int) * numRows);
    for (int i = 0; i < numRows; i++) {
        res[i] = (ptr + (i + 1) * i / 2);  // pointing array, ptr point to place where current (n) row started
                                           // => right after (n-1) stored values
        colsize[i] = i + 1;
        res[i][0] = res[i][colsize[i] - 1] = 1;  // values on the sides of the triangle is '1'
        if (i > 1)                               // from 3rd Row we start to calculate rest values of array
            for (int j = 1; j < colsize[i] - 1; j++) res[i][j] = res[i - 1][j - 1] + res[i - 1][j];
        // value of element is a sum of 2 elements above
    }
    *returnColumnSizes = colsize;
    *returnSize = numRows;
    return res;
}

void print_Pascale_triangle(int **arr, int numRows, int *colsize) {
    for (int i = 0; i < numRows; i++) {
        printf("%*c", numRows - i, ' ');
        for (int j = 0; j < colsize[i]; j++) printf("%d ", arr[i][j]);

        printf("\n");
    }
}
int main() {
    int test_arr[] = {1, 2, 3, 4, 5, 20};
    // int expected_valuse[] = {};
    int **val;
    int *val_col_sz;
    int val_sz;
    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        val = generate(test_arr[i], &val_sz, &val_col_sz);
        printf("Pascal's triangle %d rows: \n", test_arr[i]);
        print_Pascale_triangle(val, val_sz, val_col_sz);
        free(val);
        free(val_col_sz);
    }
}
