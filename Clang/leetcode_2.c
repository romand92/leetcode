#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode* next;
};

/* struct ListNode* create_list(int *arr){
    struct ListNode* head =malloc(sizeof(struct ListNode ));
    head->val = 1;
    struct ListNode* elem = head;
    for(int i =2;i<5;i++)
        {
        struct ListNode* new_elem =malloc(sizeof(struct ListNode ));
        new_elem->val = i;
        new_elem->next = NULL;

        elem->next = new_elem;
        elem = new_elem;
        }
} */
void print_struct(struct ListNode* elem);
struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2);
struct ListNode* addTwoNumbers_c(struct ListNode* l1, struct ListNode* l2);

int main() {
    int l1[] = {2, 4, 3};
    int n1 = 3;
    int l2[] = {5, 6, 4};
    int n2 = 3;
    struct ListNode* head1 = malloc(sizeof(struct ListNode));
    head1->val = l1[0];
    struct ListNode* elem = head1;
    for (int i = 1; i < n1; i++) {
        struct ListNode* new_elem = malloc(sizeof(struct ListNode));
        new_elem->val = l1[i];
        new_elem->next = NULL;

        elem->next = new_elem;
        elem = new_elem;
    }

    struct ListNode* head2 = malloc(sizeof(struct ListNode));
    head2->val = l2[0];
    elem = head2;
    for (int i = 1; i < n2; i++) {
        struct ListNode* new_elem = malloc(sizeof(struct ListNode));
        new_elem->val = l2[i];
        new_elem->next = NULL;

        elem->next = new_elem;
        elem = new_elem;
    }

    print_struct(head1);
    print_struct(head2);

    struct ListNode* res = addTwoNumbers(head1, head2);
    struct ListNode* res_c = addTwoNumbers_c(head1, head2);
    printf("\n");

    print_struct(res);
    print_struct(res_c);

    free(head1);
    free(head2);
    free(res);
    free(res_c);
}

void print_struct(struct ListNode* elem) {
    while (elem) {
        printf("%p:%d next:%p ", elem, elem->val, elem->next);
        elem = elem->next;
    }
    printf("\n");
}
struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
    struct ListNode* head_bac = malloc(sizeof(struct ListNode));
    struct ListNode* head = head_bac;
    head->val = 0;
    head->next = NULL;
    int sum = 0;
    while (1) {
        if (l1) {
            sum += l1->val;
            l1 = l1->next;
        }
        if (l2) {
            sum += l2->val;
            l2 = l2->next;
        }
        head->val = sum % 10;
        sum = sum / 10;  // sum -> rem
        // printf("%d %p %p",sum,l1,l2);
        if ((l1 != NULL) || (l2 != NULL) || sum != 0) {  // sum contains reminder
            head->next = malloc(sizeof(struct ListNode));
            head->next->next = NULL;
            // printf("%p %d %p\t",head,head->val,head->next);
            head = head->next;

        } else
            break;
    }
    return (head_bac);
}
struct ListNode* addTwoNumbers_c(struct ListNode* l1, struct ListNode* l2) {
    struct ListNode temp;
    temp.val = 0;
    temp.next = NULL;
    struct ListNode* curr = &temp;
    int remainder = 0, sum;
    while (l1 != NULL || l2 != NULL || remainder != 0) {
        sum = remainder + (l1 == 0 ? 0 : l1->val) + (l2 == 0 ? 0 : l2->val);
        remainder = sum / 10;
        sum %= 10;
        curr->next = malloc(sizeof(struct ListNode));
        curr->next->next = NULL;
        curr->next->val = sum;
        curr = curr->next;
        l1 = (l1 == 0 ? 0 : l1->next);
        l2 = (l2 == 0 ? 0 : l2->next);
    }
    return temp.next;
}