/*
2439. Minimize Maximum of Array
Medium

You are given a 0-indexed array nums comprising of n non-negative integers.

In one operation, you must:

Choose an integer i such that 1 <= i < n and nums[i] > 0.
Decrease nums[i] by 1.
Increase nums[i - 1] by 1.
Return the minimum possible value of the maximum integer of nums after performing any number of operations.

Example 1:
Input: nums = [3,7,1,6]
Output: 5
Explanation:
One set of optimal operations is as follows:
1. Choose i = 1, and nums becomes [4,6,1,6].
2. Choose i = 3, and nums becomes [4,6,2,5].
3. Choose i = 1, and nums becomes [5,5,2,5].
The maximum integer of nums is 5. It can be shown that the maximum number cannot be less than 5.
Therefore, we return 5.
*/

#include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 2

int minimizeArrayValue(int* nums, int numsSize);
long long max(long long a, long long b);
void print_arr(int* arr, int arr_sz);
int some_strange_operation(int* nums, int numsSize, int pos);

int main() {
    int test_arr[SIZEOFTESTARRAY][4] = {{3, 7, 1, 6}, {10, 1}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {4, 2};

    int expected_values[SIZEOFTESTARRAY] = {5, 10};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int val = minimizeArrayValue(test_arr[i], test_arr_lenght[i]);
        print_arr(test_arr[i], test_arr_lenght[i]);
        printf("result: %d expected: %d - %s\n", val, expected_values[i],
               (val != expected_values[i]) ? "Fail" : "Success");
    }
}

int some_strange_operation(int* nums, int numsSize, int pos) {
    int status = 1;
    if (pos < numsSize && pos > 0) {
        if (nums[pos] > 0) {
            nums[pos]--;
            nums[(pos + numsSize - 1) % numsSize]++;
        } else {
            printf("nums[%d] = %d\n", pos, nums[pos]);
            status = 0;
        }
    } else {
        printf("Wrong index picked\n");
        status = 0;
    }
    return status;
}

void print_arr(int* arr, int arr_sz) {
    printf("[");
    for (int i = 0; i < arr_sz - 1; i++) printf("%d, ", arr[i]);
    printf("%d]\n", arr[arr_sz - 1]);
}
// 3 10 11 17
// 5 10 12 17

long long max(long long a, long long b) { return (a > b) ? a : b; }

int minimizeArrayValue(int* nums, int numsSize) {
    long long pref_sum = 0, res = 0;
    for (int i = 0; i < numsSize; i++) {
        pref_sum += nums[i];
        res = max(res, (pref_sum + i) / (i + 1));
    }
    return res;
}