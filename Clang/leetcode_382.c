/*
Given a singly linked list, return a random node's value from the linked list.
Each node must have the same probability of being chosen.

Implement the Solution class:

Solution(ListNode head) Initializes the object with the head of the
singly-linked list head. int getRandom() Chooses a node randomly from the list
and returns its value. All the nodes of the list should be equally likely to be
chosen.
*/
#include <stdio.h>

//  Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode* next;
};

typedef struct {
    struct ListNode* head;
    int nodes_count;
} Solution;

Solution* solutionCreate(struct ListNode* head) {
    Solution* sol = malloc(sizeof(Solution));
    sol->head = head;
    sol->nodes_count = 0;
    while (head != NULL) {
        head = head->next;
        sol->nodes_count += 1;
    }
    // printf("%d %p\n",sol->nodes_count,sol->head);
    return sol;
}

int solutionGetRandom(Solution* obj) {
    int index = rand() % (obj->nodes_count);
    struct ListNode* head1 = obj->head;
    while (index--) head1 = head1->next;
    return head1->val;
}

void solutionFree(Solution* obj) { free(obj); }

/**
 * Your Solution struct will be instantiated and called as such:
 * Solution* obj = solutionCreate(head);
 * int param_1 = solutionGetRandom(obj);

 * solutionFree(obj);
*/
