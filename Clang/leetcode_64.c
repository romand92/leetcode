/*
Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right, which
minimizes the sum of all numbers along its path.

Note: You can only move either down or right at any point in time.
*/
#include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>
// #define SIZEOFTESTARRAY 2

int min_sol(int a, int b) {
    if (a > b)
        return b;
    else
        return a;
}

int minPathSum(int** grid, int gridSize, int* gridColSize) {  // overwrites initial grid
    for (int i = 1; i < gridSize; i++) grid[i][0] += grid[i - 1][0];
    for (int i = 1; i < *gridColSize; i++) grid[0][i] += grid[0][i - 1];
    for (int i = 1; i < gridSize; i++)
        for (int j = 1; j < gridColSize[0]; j++) {
            grid[i][j] += min_sol(grid[i - 1][j], grid[i][j - 1]);
        }
    return grid[gridSize - 1][*gridColSize - 1];
}

int main() {
    int test_arr[3][3] = {{1, 3, 1}, {1, 5, 1}, {4, 2, 1}};
    int* test_arr_ptr[3];
    int* ptr = test_arr[0];
    for (int i = 0; i < 3; i++) test_arr_ptr[i] = ptr + i * 3;  // test_arr -> **ptr
    int test_arr_col_sz[3] = {3, 3, 3};
    int expected_values[] = {7};

    int val = minPathSum(test_arr_ptr, 3, test_arr_col_sz);
    printf("%d expected:%d %s\n", val, expected_values[0], (val != expected_values[0]) ? "Fail" : "Success");
}
