/*
3. Longest Substring Without Repeating Characters
Medium
Given a string s, find the length of the longest
substring without repeating characters.
*/

#include <stdio.h>
#define SIZEOFTESTARRAY 5

int is_char_repeats(char *str, int index_start, int index_end, unsigned int len);
int is_last_char_repeats_in_str(char *str, int index_start, int index_end, unsigned int len);
unsigned int _strlen(char *str);

int lengthOfLongestSubstring_expand(
    char *s);  // realisation with expanding string and checks if new char was in string before
int lengthOfLongestSubstring(char *s);  // realisation with hashtable(0ms,100%)

int main() {
    char test_arr[SIZEOFTESTARRAY][12] = {"aba", "pwwkew", "aabcd", "abcybee", "0123451670a"};
    int expected_values[SIZEOFTESTARRAY] = {2, 3, 4, 4, 9};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int val = lengthOfLongestSubstring(test_arr[i]);
        // int val = lengthOfLongestSubstring_expand(test_arr[i]);
        printf("\"%s\"; len:%d expected:%d %s\n", test_arr[i], val, expected_values[i],
               (val != expected_values[i]) ? "Fail" : "Success");
    }
}

int lengthOfLongestSubstring(char *s) {
    int Len = _strlen(s);
    int hash_t[95] = {0};  // space - 32, '~' - 126
    int last_seen_idx[95] = {-1};

    int start = 0;
    int old_start = 0;
    int max_len = 0, len;
    for (int i = 0; i < Len; i++) {
        int idx = s[i] - ' ';    // calculate index
        if (hash_t[idx] == 1) {  // if s[i] was seen before
            len = i - start;     // calculate len of current word
            if (len > max_len) max_len = len;
            // printf("triger:%c;idx:%d;start:%d; len:%d; ",s[i],i, start,len);
            old_start = start;
            start = last_seen_idx[idx] + 1;  // set new start next to same symbol
            // printf("; last_seen:%d; new start:%d;",last_seen_idx[idx],start);
            // printf("\n");

            for (int j = old_start; j < start;
                 j++) {  //<-all from old start till new start should be excluded
                int idx_j = s[j] - ' ';
                hash_t[idx_j] = 0;  //--
                // printf("-%c",s[j]);
                last_seen_idx[idx_j] = -1;
            }
        }

        hash_t[idx] = 1;
        last_seen_idx[idx] = i;
        //}
        /*else if(hash_t[idx] > 1){
            printf("Have no idea why am I here ¯\\_(ツ)_/¯");
        }*/
    }
    int len_of_last_string = Len - start;
    return (len_of_last_string > max_len) ? len_of_last_string : max_len;
}

int lengthOfLongestSubstring_expand(char *s) {
    unsigned int longest = 1;
    unsigned int len = _strlen(s);
    for (int i = 0; i < len; i++) {
        if (!is_char_repeats(s, i, i + longest, len))
            while (((i + longest) < len) && !is_last_char_repeats_in_str(s, i, i + longest, len)) {
                longest++;
                // printf(" !%d! ",longest);
            };
    }
    return longest;
}

unsigned int _strlen(char *str) {
    unsigned int size = 0;
    while (*str++) {
        size++;
    }
    return size;
}

int is_char_repeats(char *str, int index_start, int index_end, unsigned int len) {
    int fount = 0;
    int end = len - 1 - index_end;
    if (((index_end - index_start) < 1) || (end < 0)) return 0;
    for (str += index_start; *(str + end); str++) {
        for (char *str_bac = str + 1; *(str_bac + end); str_bac++) {
            if (*str == *str_bac) {
                // printf("%c,%c;",*str,*str_bac);
                fount = 1;
                break;
            }  // printf("%c,%c;",*str,*str_bac);
        }
        // printf("\n");
        if (fount) break;
    }
    return fount;
}

int is_last_char_repeats_in_str(char *str, int index_start, int index_end, unsigned int len) {
    int end = len - index_end;  // exclude last symbol
    if (((index_end - index_start) < 1) || (end < 1)) return 0;
    char last_sym = *(str + index_end);  // probably pointer faster
    int repeats = 0;
    // printf("Last:%c\n", last_sym);
    for (str += index_start; *(str + end); str++) {
        //  printf("%c",*str);
        if (*str == last_sym) {
            repeats = 1;
            break;
        }
    }
    // printf("\nchar after cycle: %c \n",*str);
    return repeats;
}