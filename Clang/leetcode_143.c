/*
143. Reorder List
Medium
You are given the head of a singly linked-list. The list can be represented as:

L0 → L1 → … → Ln - 1 → Ln
Reorder the list to be on the following form:

L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
You may not modify the values in the list's nodes. Only nodes themselves may be changed.
*/

#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>

#include "ListNode.c"
#define SIZEOFTESTARRAY 2
#define MAX_NODELIST_SIZE 50000

void reorderList(struct ListNode* head) {
    struct ListNode *arr[MAX_NODELIST_SIZE], *head_bac = head;
    int nodes = 0;
    while (head) {
        arr[nodes++] = head;
        head = head->next;
    }
    for (int i = 0; i < nodes / 2; i++) {
        arr[i]->next = arr[nodes - i - 1];
        arr[nodes - i - 1]->next = arr[i + 1];
    }
    // if (nodes%2 == 0)
    arr[nodes / 2]->next = NULL;
    // else
    //     arr[(nodes)/2] -> next = NULL;
    // return arr[0];
}

int main() {
    int test_arr[SIZEOFTESTARRAY][5] = {{1, 2, 3, 4}, {1, 2, 3, 4, 5}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {4, 5};
    // int test_var1[SIZEOFTESTARRAY] = {};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        struct ListNode* list = init_list_from_arr(test_arr[i], test_arr_lenght[i]);
        printf("List:\n");
        print_list(list);
        reorderList(list);
        printf("Reordered List:\n");
        print_list(list);
        destroy_list(list);
        printf("\n");
    }
}
