/*
Given the root of a binary tree, determine if it is a complete binary tree.

In a complete binary tree, every level, except possibly the last, is completely filled, and all nodes in the
last level are as far left as possible. It can have between 1 and 2h nodes inclusive at the last level h.
*/
#include <stdbool.h>
#include <stdio.h>
// #include <stdlib.h>
// #define SIZEOFTESTARRAY 4

//  Definition for a binary tree node.
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

void enqueue(struct TreeNode **queue, int *rear, int *front, struct TreeNode *item) {
    if (*front == -1) *front = 0;
    *rear += 1;
    queue[*rear] = item;
}

struct TreeNode *dequeue(struct TreeNode **queue, int *rear, int *front) {
    *front += 1;
    return queue[*front - 1];
}

bool isCompleteTree(struct TreeNode *root) {
    bool compleete = true;
    struct TreeNode *q1[1000] = {NULL};
    int front1 = -1, rear1 = -1;
    enqueue(q1, &rear1, &front1, root);
    struct TreeNode *item1;
    while (rear1 >= front1 && compleete) {
        int i = rear1;
        while (q1[i] == NULL && i > front1)  // find first not NULL in queue
            i--;
        if (i > front1)  // if find NULL before ^^ => return false
            for (; i >= front1 && compleete; i--) {
                if (q1[i] == NULL) compleete = false;
            }
        item1 = dequeue(q1, &rear1, &front1);
        // printf("vals:%d %d ", item1->val,item2->val);
        if (item1 && compleete) {
            enqueue(q1, &rear1, &front1, item1->left);
            enqueue(q1, &rear1, &front1, item1->right);
        }
    }

    return compleete;
}

int main() {
    // int test_arr[][] = {};
    // int test_arr_lenght[] = {};
    // int test_var1[] = {};

    // int expected_values[] = {};
    // int sizeOfTestArr = sizeof(test_arr) / sizeof(test_arr[0]);

    // for (int i = 0; i < 3; i++) {
    //     int val = minEatingSpeed(test_arr[i], test_arr_lenght[i], test_h[i]);
    //     printf("%d expected:%d %s\n", val, expected_valuse[i],
    //            (val != expected_valuse[i]) ? "Fail" : "Success");
    // }
}
