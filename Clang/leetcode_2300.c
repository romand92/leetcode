/*
2300. Successful Pairs of Spells and Potions

You are given two positive integer arrays spells and potions, of length n and m respectively, where spells[i]
represents the strength of the ith spell and potions[j] represents the strength of the jth potion.

You are also given an integer success. A spell and potion pair is considered successful if the product of
their strengths is at least success.
*/

#include <stdio.h>
// #include <stdbool.h>
#include <math.h>
#include <stdlib.h>
#define SIZEOFTESTARRAY 2

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */

int comp(const void* a, const void* b) { return *(int*)a - *(int*)b; }
// comparator for stdlib.qsort
int isSpellSuccessfull(long long success, int spell, int potion) {
    return ((long long)spell * potion >= success) ? 1 : 0;
}
int countPairs(int* potions, int potionsSize, int target) {
    int max = potionsSize - 1;
    int min = 0;
    int mid;
    while (max > min) {
        mid = min + (max - min) / 2;
        if (potions[mid] >= target) {
            max = mid;
        } else {
            min = mid + 1;
        }
    }
    return potionsSize - min;
}

int find_first_successfull_spell(int max_spell, int max_posion, long long success) {
    int min = 1;
    int mid;
    while (min < max_spell) {
        mid = min + (max_spell - min) / 2;
        if (isSpellSuccessfull(success, mid, max_posion)) {
            max_spell = mid;
        } else {
            min = mid + 1;
        }
    }
    return (isSpellSuccessfull(success, min, max_posion)) ? min : 0;
}  // return min integer from 1...10^5 starts from we can find t least 1 successfull spell

int* successfulPairs(int* spells, int spellsSize, int* potions, int potionsSize, long long success,
                     int* returnSize) {
    int* successfulPairs = calloc(spellsSize, sizeof(int));
    *returnSize = spellsSize;
    // qsort(spells,spellsSize,sizeof(int),comp);
    qsort(potions, potionsSize, sizeof(int), comp);

    int min_spell = find_first_successfull_spell(10e5, potions[potionsSize - 1], success);
    if (!min_spell) return successfulPairs;

    for (int i = 0; i < spellsSize; i++) {
        if (spells[i] >= min_spell) {
            int target = ceil((double)success / (double)spells[i]);
            successfulPairs[i] = countPairs(potions, potionsSize, target);
        }
        // else //if current spell < min_spell don't bother ourselves with bin search
        //     successfulPairs[i] = 0;
    }
    return successfulPairs;
}

int main() {
    int test_spells[SIZEOFTESTARRAY][5] = {{5, 1, 3}, {3, 1, 2}};
    int test_spells_lenght[SIZEOFTESTARRAY] = {3, 3};
    int test_potions[SIZEOFTESTARRAY][5] = {{1, 2, 3, 4, 5}, {8, 5, 8}};
    int test_potions_lenght[SIZEOFTESTARRAY] = {5, 3};
    long long test_sucess[SIZEOFTESTARRAY] = {7, 16};

    int expected_values[SIZEOFTESTARRAY][5] = {{4, 0, 3}, {2, 0, 2}};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int val_sz = 0;
        int* val = successfulPairs(test_spells[i], test_spells_lenght[i], test_potions[i],
                                   test_potions_lenght[i], test_sucess[i], &val_sz);
        int res = 1;
        for (int j = 0; j < val_sz; j++) {
            printf("%d ", val[j]);
            if (val[j] != expected_values[i][j]) res = 0;
        }
        printf("%s\n", (res) ? "Success" : "Fail");
        // printf("%d expected:%d %s\n", val, expected_values[i],
        //        (val != expected_values[i]) ? "Fail" : "Success");
    }
}
