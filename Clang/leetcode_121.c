/*
You are given an array prices where prices[i] is the price of a given stock on the ith day.

You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the
future to sell that stock.

Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.
*/
#include <stdio.h>

// int copmpare (const void *a1, const void *a2){
//     return *(int *)a2 - *(int *)a1;
// }

int maxProfit(int* prices, int pricesSize) {
    int max_profit = 0;
    int min_price = prices[0];  // INT_MAX;
    int profit;
    for (int i = 1; i < pricesSize; i++) {
        if (min_price > prices[i]) {
            min_price = prices[i];
        } else {
            profit = prices[i] - min_price;
            if (profit > max_profit) max_profit = profit;
        }
    }

    /*
     qsort from end
     int i = pricesSize - 2; // 2nd element from end
     int profit = 0;
     for(;i>=0;i--){
         qsort(prices + i + 1, pricesSize - i - 1,sizeof(int), copmpare);
         profit = prices[i+1] - prices[i];
         if (profit > max_profit)
             max_profit = profit;
     }
     for(int k=0; k < pricesSize;k++ )
         printf("%d ",prices[k]);
      */

    return max_profit;
}

int main() {
    int test_arr[][6] = {{7, 1, 5, 3, 6, 4}, {7, 6, 4, 3, 1, 0}};
    int test_arr_lenght[] = {6, 5};
    // int test_var1[] = {};
    int expected_valuse[] = {5, 0};
    int testArrRows = sizeof(test_arr) / sizeof(test_arr[0]);

    for (int i = 0; i < testArrRows; i++) {
        int val = maxProfit(test_arr[i], test_arr_lenght[i]);
        printf("%d expected: %d %s\n", val, expected_valuse[i],
               (val != expected_valuse[i]) ? "Fail" : "Success");
    }
}