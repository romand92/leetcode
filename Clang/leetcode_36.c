/*
Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the
following rules:

Each row must contain the digits 1-9 without repetition.
Each column must contain the digits 1-9 without repetition.
Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.
Note:

A Sudoku board (partially filled) could be valid but is not necessarily solvable.
Only the filled cells need to be validated according to the mentioned rules.
*/
#include <stdbool.h>
#include <stdio.h>

bool isValidSudoku(char** board, int boardSize, int* boardColSize) {
    // check Rows
    for (int i = 0; i < boardSize; i++)
        for (int j = 0; j < boardColSize[i] - 1; j++)
            for (int k = j + 1; k < boardColSize[i]; k++)
                if (board[i][j] != '.')
                    if (board[i][j] == board[i][k]) return false;

    // check Cols
    int row_i = 0;
    for (int j = 0; j < boardColSize[0]; j++) {  // boardColSize[0]!!!
        for (row_i = 0; row_i < boardSize - 1; row_i++) {
            // printf("%d [%d,%d]", j ,row_i,j);
            for (int k = row_i + 1; k < boardSize; k++) {
                if (board[row_i][j] != '.')
                    if (board[row_i][j] == board[k][j]) return false;
            }
            // printf("\n");
        }
    }

    int sq_j_base = 3, sq_i_base = 3;
    int squares = 3;  // sqrt(boardColSize[0] * boardSize / (sq_j_base * sq_i_base));
    for (int sq_i = 0; sq_i < squares; sq_i++)
        for (int sq_j = 0; sq_j < squares; sq_j++) {
            for (int i = 0; i < sq_j_base * sq_i_base - 1; i++)
                for (int j = i + 1; j < sq_j_base * sq_i_base; j++)
                    if (board[sq_i * sq_i_base + i / sq_i_base][sq_j * sq_j_base + i % sq_j_base] != '.')
                        if (board[sq_i * sq_i_base + i / sq_i_base][sq_j * sq_j_base + i % sq_j_base] ==
                            board[sq_i * sq_i_base + j / sq_i_base][sq_j * sq_j_base + j % sq_j_base])
                            return false;
        }
    return true;
}

void print_Sudoku_board(char** board, int boardSize, int* boardColSize) {
    for (int i = 0; i < boardSize; i++) {
        for (int j = 0; j < boardColSize[i]; j++) printf("%c ", board[i][j]);
        printf("\n");
    }
}

int main() {
    char test_arr[9][9] = {
        {'5', '3', '.', '.', '7', '.', '.', '.', '.'}, {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
        {'.', '9', '8', '.', '.', '.', '.', '6', '.'}, {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
        {'4', '.', '.', '8', '.', '3', '.', '.', '1'}, {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
        {'.', '6', '.', '.', '.', '.', '2', '8', '.'}, {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
        {'.', '.', '.', '.', '8', '.', '.', '7', '9'}};

    char* test_arr_ptr[9];
    for (int i = 0; i < 9; i++) test_arr_ptr[i] = test_arr[i];

    char test_arr2[9][9] = {
        {'.', '.', '4', '.', '.', '.', '6', '3', '.'}, {'.', '.', '.', '.', '.', '.', '.', '.', '.'},
        {'5', '.', '.', '.', '.', '.', '.', '9', '.'}, {'.', '.', '.', '5', '6', '.', '.', '.', '.'},
        {'4', '.', '3', '.', '.', '.', '.', '.', '1'}, {'.', '.', '.', '7', '.', '.', '.', '.', '.'},
        {'.', '.', '.', '5', '.', '.', '.', '.', '.'}, {'.', '.', '.', '.', '.', '.', '.', '.', '.'},
        {'.', '.', '.', '.', '.', '.', '.', '.', '.'}};
    char* test_arr_ptr2[9];
    for (int i = 0; i < 9; i++) test_arr_ptr2[i] = test_arr2[i];
    int boardsize[9] = {9, 9, 9, 9, 9, 9, 9, 9, 9};
    bool expected[2] = {true, false};

    print_Sudoku_board(test_arr_ptr, 9, boardsize);
    bool res = isValidSudoku(test_arr_ptr, 9, boardsize);
    printf("Board is %svalid\n\n", ((res == true) ? "" : "in"));

    print_Sudoku_board(test_arr_ptr2, 9, boardsize);
    res = isValidSudoku(test_arr_ptr2, 9, boardsize);
    printf("Board is %svalid\n\n", ((res == true) ? "" : "in"));
}
