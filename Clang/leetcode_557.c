/*
557. Reverse Words in a String III

Given a string s, reverse the order of characters in each word within a sentence while still preserving
whitespace and initial word order.
*/

#include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 5

void reverseString(char *s, int sSize) {
    char *start = s, *end = s + sSize - 1;
    while (start < end) {
        char tmp = *start;
        *start = *end;
        *end = tmp;
        start++;
        end--;
    }
}

char *reverseWords(char *s) {
    char *wordStart = s;
    char *wordEnd = s;
    while (*(wordEnd)) {
        if (*(wordEnd) == ' ') {
            reverseString(wordStart, wordEnd - wordStart);
            wordStart = wordEnd + 1;
        }
        wordEnd++;
    }
    reverseString(wordStart, wordEnd - wordStart);
    return s;
}

int main() {
    char test_arr[SIZEOFTESTARRAY][34] = {"Let's take LeetCode contest", "God Ding",
                                          "a sentence while still preserving", "A", ""};

    // int expected_values[SIZEOFTESTARRAY] = {};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        printf("Dirrect:  \"%s\"\n", test_arr[i]);
        char *val = reverseWords(test_arr[i]);
        printf("Reversed: \"%s\"\n\n", val);
    }
}
