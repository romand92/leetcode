/*
Given the head of a singly linked list, reverse the list, and return the reversed list.
*/
#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#define SIZEOFTESTARRAY 3

// Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode* next;
};

struct ListNode* reverseList_iter(struct ListNode* head) {
    struct ListNode* next;  // = head->next;
    struct ListNode* prev = NULL;
    while (head) {
        next = head->next;
        head->next = prev;
        prev = head;
        head = next;
    }
    return prev;
}

struct ListNode* reverseList_recursive(struct ListNode* head, struct ListNode* prev) {
    if (!head) return head;
    struct ListNode* next = head->next;
    head->next = prev;
    if (!next) return head;
    return reverseList_recursive(next, head);
}

// struct ListNode* reverseList(struct ListNode* head){
//     return reverseList_recursive(head,NULL);

// }
struct ListNode* list_init_arr(int* val, int val_size) {
    struct ListNode *head = NULL, *next = NULL;

    for (int i = val_size; i > 0; i--) {
        head = malloc(sizeof(struct ListNode));
        head->val = val[i];
        head->next = next;
        next = head;
    }
}

void destroy_list(struct ListNode* head) {
    struct ListNode* tmp;
    while (head) {
        tmp = head;
        head = head->next;
        free(tmp);
    }
}

void print_list(struct ListNode* head) {
    if (!head) printf("NULL\n");
    while (head) {
        printf("%d%s", head->val, (head->next) ? " -> " : "\n");
        head = head->next;
    }
}

int main() {
    int test_arr[SIZEOFTESTARRAY][5] = {{1, 2, 3, 4, 5}, {1, 2}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {5, 2, 0};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        struct ListNode* head = list_init_arr(test_arr[i], test_arr_lenght[i]);
        print_list(head);
        destroy_list(head);
    }
}
