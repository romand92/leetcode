/*
977. Squares of a Sorted Array

Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number
sorted in non-decreasing order.
*/

#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#define SIZEOFTESTARRAY 8

// int comp(const void *a,const void *b){
// return *(int*)a - *(int*)b;
// }
// int* sortedSquares(int* nums, int numsSize, int* returnSize){
//     int * res = malloc(sizeof(int)*numsSize);
//     for (int i = 0; i < numsSize; i++)
//         res[i] = nums[i]*nums[i];
//     qsort(res,numsSize,sizeof(int),comp);
//     *returnSize = numsSize;
//     return res;
// }

int* sortedSquares(int* nums, int numsSize, int* returnSize) {
    int* res = malloc(sizeof(int) * numsSize);
    *returnSize = numsSize;
    int *start = nums, *end = nums + numsSize - 1;
    for (int k = numsSize - 1; k >= 0; k--) {
        res[k] = (*end) * (*end);
        if (res[k] < ((*start) * (*start))) {
            res[k] = (*start) * (*start);
            start++;
        } else {
            end--;
        }
    }
    return res;
}

void print_arr(int* arr, int size) {
    for (int i = 0; i < size; i++) printf("%d ", arr[i]);
}

int main() {
    int test_arr[SIZEOFTESTARRAY][5] = {{-4, -1, 0, 3, 10},
                                        {-7, -3, 2, 3, 11},
                                        {-4, -3, -2, -1},
                                        {-3, -2, -1, 0},
                                        {0, 1, 2, 3},
                                        {1, 2, 3, 4},
                                        {1},
                                        {-1, 2}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {5, 5, 4, 4, 4, 4, 1, 2};

    // int expected_values[SIZEOFTESTARRAY] = {};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int val_sz = 0;
        int* val = sortedSquares(test_arr[i], test_arr_lenght[i], &val_sz);
        printf("Initial array : ");
        print_arr(test_arr[i], test_arr_lenght[i]);
        printf("\tResult array : ");
        print_arr(val, val_sz);
        printf("\n");
        free(val);
    }
}
