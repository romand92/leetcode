/*
700. Search in a Binary Search Tree
Easy

You are given the root of a binary search tree (BST) and an integer val.

Find the node in the BST that the node's value equals val and return the subtree rooted with that node. If
such a node does not exist, return null.
*/
#include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>
// #define SIZEOFTESTARRAY 4

// Definition for a binary tree node.
struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

struct TreeNode* searchBST(struct TreeNode* root, int val) {
    struct TreeNode* head = root;
    while (head && head->val != val) {
        if (head->val < val)
            head = head->right;
        else
            head = head->left;
    }
    return head;
}

int main() {}
