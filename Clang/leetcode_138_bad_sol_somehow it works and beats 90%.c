/*
A linked list of length n is given such that each node contains an additional random pointer, which could
point to any node in the list, or null.

Construct a deep copy of the list. The deep copy should consist of exactly n brand new nodes, where each new
node has its value set to the value of its corresponding original node. Both the next and random pointer of
the new nodes should point to new nodes in the copied list such that the pointers in the original list and
copied list represent the same list state. None of the pointers in the new list should point to nodes in the
original list.

For example, if there are two nodes X and Y in the original list, where X.random --> Y, then for the
corresponding two nodes x and y in the copied list, x.random --> y.

Return the head of the copied linked list.

The linked list is represented in the input/output as a list of n nodes. Each node is represented as a pair of
[val, random_index] where:

val: an integer representing Node.val
random_index: the index of the node (range from 0 to n-1) that the random pointer points to, or null if it
does not point to any node. Your code will only be given the head of the original linked list.
*/
#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#define SIZEOFTESTARRAY 2

// * Definition for a Node.
struct Node {
    int val;
    struct Node* next;
    struct Node* random;
};

struct Node* node_init(int val) {
    struct Node* item = malloc(sizeof(struct Node));
    item->val = val;
    item->random = NULL;
    return item;
}

struct Node* init_list_from_arr(int* arr, const int arr_sz, int* arr_rand) {
    struct Node* head = NULL;
    struct Node** arr_ptr = malloc(sizeof(struct Node*) * arr_sz);
    for (int i = arr_sz - 1; i >= 0; i--) {
        struct Node* item = node_init(arr[i]);
        arr_ptr[i] = item;
        item->next = head;
        head = item;
    }
    struct Node* head_tmp = head;
    for (int i = 0; i < arr_sz; i++) {
        if (arr_rand[i] == -1)
            head->random = NULL;
        else
            head->random = arr_ptr[arr_rand[i]];
        head = head->next;
    }
    free(arr_ptr);

    return head_tmp;
}

void print_list(struct Node* head) {
    struct Node* head_tmp = head;
    char buf[128];
    while (head) {
        printf("ADDR:%p VAL:%d\tRAND_ADDR:VAL|%p:%s\n", head, head->val, head->random,
               (head->random) ? itoa(head->random->val, buf, 10) : "NULL");
        head = head->next;
    }
}

void destroy_list(struct Node* head) {
    while (head) {
        struct Node* del = head;
        head = head->next;
        free(del);
    }
}

struct Node* copyRandomList(struct Node* head) {
    struct Node* prev = NULL;
    struct Node* random[1000];  // kinda hastab
    struct Node* new[1000];
    struct Node* old[1000];
    struct Node* copy_head;
    int node_count = 0;
    while (head) {
        struct Node* copy_item = node_init(head->val);
        new[node_count] = copy_item;
        old[node_count] = head;
        if (node_count == 0) copy_head = copy_item;
        if (prev) prev->next = copy_item;
        random[node_count] = head->random;

        prev = copy_item;
        head = head->next;
        if (!head) copy_item->next = NULL;
        node_count++;
    }
    for (int i = 0; i < node_count; i++) {
        for (int j = 0; j < node_count; j++) {
            if (random[i] == old[j]) new[i]->random = new[j];
        }
    }

    return copy_head;
}

int main() {
    int test_arr[SIZEOFTESTARRAY][5] = {{7, 13, 11, 10, 1}, {3, 3, 3}};
    int test_arr_rand[SIZEOFTESTARRAY][5] = {{-1, 0, 4, 2, 0}, {-1, 1, -1}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {5, 3};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        struct Node* head = init_list_from_arr(test_arr[i], test_arr_lenght[i], test_arr_rand[i]);
        struct Node* head_cpy = copyRandomList(head);

        printf("Init List:\n");
        print_list(head);
        printf("Copied List:\n");
        print_list(head_cpy);
        printf("%s\n", "----------------------------------------------------");
        destroy_list(head);
        destroy_list(head_cpy);
    }
}
