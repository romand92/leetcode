/*
71. Simplify Path
Medium
Given a string path, which is an absolute path (starting with a slash '/') to a file or directory in a
Unix-style file system, convert it to the simplified canonical path.

In a Unix-style file system, a period '.' refers to the current directory, a double period '..' refers to the
directory up a level, and any multiple consecutive slashes (i.e. '//') are treated as a single slash '/'. For
this problem, any other format of periods such as '...' are treated as file/directory names.

The canonical path should have the following format:

The path starts with a single slash '/'.
Any two directories are separated by a single slash '/'.
The path does not end with a trailing '/'.
The path only contains the directories on the path from the root directory to the target file or directory
(i.e., no period '.' or double period '..') Return the simplified canonical path.

*add solution without strtok
*/

#include <stdio.h>
#include <string.h>
// #include <stdbool.h>
#include <stdlib.h>
#define SIZEOFTESTARRAY 3

char *append(char *dest, char *src, int *size);
char *simplifyPath_strtok(char *path);

int main() {
    char test_arr[SIZEOFTESTARRAY][15] = {"/home/", "home/../.././", "/home///./foo/"};

    char expected_values[SIZEOFTESTARRAY][10] = {"/home", "/", "/home/foo"};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        char *res = simplifyPath_strtok(test_arr[i]);
        printf("res: \"%s\" expected:\"%s\" %s\n", res, expected_values[i],
               (strcmp(res, expected_values[i]) != 0) ? "Fail" : "Success");
        free(res);
    }
}

char *append(char *dest, char *src, int *size) {
    *(dest++) = '/';
    (*size)++;
    for (; *src; src++, dest++, (*size)++) *dest = *src;
    *dest = '\0';
    return dest;
}

char *simplifyPath_strtok(char *path) {
    char *folders[1000];

    char *leks = strtok(path, "/");
    int i = 0;
    while (leks != NULL) {
        // printf("\n%s", leks);
        if (strcmp(leks, "..") == 0) {  // remove last folder
            i--;
            if (i < 0) i = 0;
        } else if (strcmp(leks, ".") == 0)
            ;  // skip
        else
            folders[i++] = leks;
        leks = strtok(NULL, "/");
    }
    char *res = malloc(sizeof(char) * 3001);
    int size = (i > 0) ? 0 : 1;  // if no folder will be appended threre is 2 symbols, otherwise folder will
                                 // be appended with first '/' before name errasing initial '/'
    res[0] = '/';
    res[1] = '\0';
    char *res_ptr = res;
    for (int j = 0; j < i; j++) {
        res_ptr = append(res_ptr, folders[j], &size);
        // printf("\n%s",folders[j]);
    }
    res = realloc(res, sizeof(char) * ++size);  // ++size for terminating symbol
    // printf("\n\n%s %d",res, size);
    return res;
}