/*
2085. Count Common Words With One Occurrence
Easy

Given two string arrays words1 and words2, return the number of strings that appear exactly once in each of
the two arrays.
*/
#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#include <string.h>
// #include <crtdbg.h>

#include "ht_key-str_val-int.c"
#include "leetcode_2085.h"  // defined test arrays

#define CAPACITY 1000  // Size of the HashTable.

int HT_decrease_val(struct HT* ht, char* word) {
    int status = 0;
    HT_item* curr_item = HT_find(ht, word);
    if (curr_item && curr_item->word && curr_item->val <= 1) {
        if (strcmp(curr_item->word, word) == 0) {
            curr_item->val -= 1;
            status = 1;
        }
    }

    return status;
}

int countWords(char** words1, int words1Size, char** words2, int words2Size) {
    int res = 0;
    HT* ht = create_table(CAPACITY + 1);
    int k = 0;
    for (int i = 0; i < words1Size; i++, k++) HT_add(ht, words1[i]);

    // print_table(ht);
    // for (int i =0; i< ht->size;i++){
    //     if(ht->words[i] && ht->words[i]->val != 1)
    //         free_item(ht->words[i]);
    // }

    for (int i = 0; i < words2Size; i++) HT_decrease_val(ht, words2[i]);

    for (int i = 0; i < CAPACITY; i++) {
        if (ht->words[i]) {
            if (ht->words[i]->val == 0) {
                printf("%d %s %d\n", i, ht->words[i]->word, ht->words[i]->val);
                res++;
            }
        }
    }

    free_table(ht);

    return res;
}

int main() {
    char* words1[] = words1def;
    char* words2[] = words2def;
    int words2Size = sizeof(words2) / sizeof(char*);
    int words1Size = sizeof(words1) / sizeof(char*);
    // printf("words1:%d, words2:%d\n", words1Size, words2Size);
    int expected_value = 8;
    int val = countWords(words1, words1Size, words2, words2Size);
    printf("%d expected:%d %s\n", val, expected_value, (val != expected_value) ? "Fail" : "Success");
}
