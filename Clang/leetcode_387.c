/*
Given a string s, find the first non-repeating character in it and return its index. If it does not exist,
return -1.
*/
#include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 4

int firstUniqChar(char *s) {
    char *i = s;
    int pos = 0;
    int repeat = 0;
    for (; *i; i++, pos++) {
        repeat = 0;
        for (char *j = s; *j; j++)
            if (i != j && *i == *j) {
                // printf("%c:%c ",*i,*j);
                repeat = 1;
                break;
            }
        if (!repeat) {
            // printf("%c no repeat", *i);
            // pos++;
            //     for (;*s != *i;s++,pos++)
            //         ;
            break;
        }
    }
    if (repeat || *s == '\0') pos = -1;
    return pos;
}

int firstUniqChar_hashtab(char *s) {
    int hashtab[26] = {0};
    char *i = s;
    while (*i) {
        hashtab[*i - 'a']++;
        i++;
    }
    int j = 0;
    while (*(s + j)) {
        if (hashtab[*(s + j) - 'a'] == 1) return j;
        j++;
    }
    return -1;
}

int main() {
    char test_arr[SIZEOFTESTARRAY][10] = {"aabb", "aab", "aaabbcbbb", ""};
    int expected_values[] = {-1, 2, 5, -1};
    // int sizeOfTestArr = sizeof(test_arr) / sizeof(test_arr[0]);

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int val = firstUniqChar_hashtab(test_arr[i]);
        printf("\"%s\" %d expected:%d %s\n", test_arr[i], val, expected_values[i],
               (val != expected_values[i]) ? "Fail" : "Success");
    }
    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int val = firstUniqChar(test_arr[i]);
        printf("\"%s\" %d expected:%d %s\n", test_arr[i], val, expected_values[i],
               (val != expected_values[i]) ? "Fail" : "Success");
    }
}
