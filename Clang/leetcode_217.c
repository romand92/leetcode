/*
Given an integer array nums, return true if any value appears at least twice in
the array, and return false if every element is distinct.
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define test_arr_lenght 50000

int compare(const void *arg1, const void *arg2) { return *(int *)arg1 - *(int *)arg2; }

bool containsDuplicate(int *nums, int numsSize) {
    qsort(nums, numsSize, sizeof(int), compare);
    for (int i = 0; i < numsSize - 1; i++)
        if (nums[i] == nums[i + 1]) return true;
    return false;
}

int main() {
    int test_arr[test_arr_lenght];
    for (int i = 0; i < test_arr_lenght; i++) test_arr[i] = i - 23484;
    int expected_value = false;

    // for (int i = 0; i < 3; i++) {
    int val = containsDuplicate(test_arr, test_arr_lenght);
    printf("%s expected:%s %s\n", (val) ? "true" : "false", (expected_value) ? "true" : "false",
           (val != expected_value) ? "Fail" : "Success");
    // }
}
