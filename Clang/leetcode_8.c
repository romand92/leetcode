#include <stdio.h>

// Given an array arr of positive integers sorted in a strictly increasing
// order, and an integer k. Return the kth positive integer that is missing from
// this array.

int findKthPositive(int* arr, int arrSize, int k) {
    int miss_c = 0;
    int arr_i = 0;
    int i = 1;
    for (; arr_i < arrSize; i++) {
        if (arr[arr_i] != i) {
            if (++miss_c == k) {
                i++;
                break;
            }
        } else
            arr_i++;
    }

    return ((arr_i == arrSize) ? (i - 1 + k - miss_c) : (i - 1));
}  //^^^^if array edns before member fount just add (missing ammount) * i;

int main() {
    int test_arr1[] = {2, 3, 4, 7, 11}, test_arr2[] = {1, 2, 3, 4};
    int* test_arr[2] = {test_arr1, test_arr2};
    int test_k[] = {5, 2};
    int test_arrSize[] = {5, 4};

    for (int i = 0; i < 2; i++) printf("%d\n", findKthPositive(test_arr[i], test_arrSize[i], test_k[i]));
}