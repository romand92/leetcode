#include <stdio.h>

int _myAtoi(char* s) {
    int max_int = 2147483647;
    int max_r = 9;
    // printf("%d",max_int*-1);
    while (*s && *s == ' ') s++;
    // printf("%s",s);
    int sign = 1;
    if (*s == '-') {
        sign = -1;
        s++;
    } else if (*s == '+')
        s++;
    unsigned int res = 0;
    int overflow = 0;
    char* start = s;
    while (*s <= '9' && *s >= '0') {
        if (s - start < max_r || (s - start == max_r && res <= max_int / 10 && (*s < '8'))) {
            res = res * 10 + (*s - '0');
        } else {
            overflow = 1;
            break;
        }

        s++;
    }
    // printf("digits: %d ",s-start);
    if (overflow) {
        res = (sign > 0) ? max_int : -1 - max_int;
    } else if (sign < 0)
        res = -res;

    return res;
}

int main() {
    printf("\n%d", _myAtoi("-2147483647"));
    printf("\n%d", _myAtoi("-2147483648"));
    printf("\n%d", _myAtoi("2147483649"));
    printf("\n%d", _myAtoi("-21474836490"));
    printf("\n%d", _myAtoi("2147483647"));
    printf("\n%d", _myAtoi("21474836470"));
    printf("\n%d", _myAtoi("-2147"));
    printf("\n%d", _myAtoi("2147"));
}