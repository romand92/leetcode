/*

*/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

//#define SIZEOFTESTARRAY 4
// Definition for a binary tree node.
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

bool orderTraversalCheck(struct TreeNode *left, struct TreeNode *right) {
    if (!left && !right) return true;
    if (!left || !right) return false;

    if (!orderTraversalCheck(left->left, right->right)) return false;
    if (!orderTraversalCheck(left->right, right->left)) return false;
    return (left->val == right->val);
}

bool isSymmetric_(struct TreeNode *root) { return orderTraversalCheck(root->left, root->right); }

void enqueue(struct TreeNode **queue, int *rear, int *front, struct TreeNode *item) {
    if (*front == -1) *front = 0;
    *rear += 1;
    queue[*rear] = item;
}

struct TreeNode *dequeue(struct TreeNode **queue, int *rear, int *front) {
    *front += 1;
    return queue[*front - 1];
}

bool isSymmetric(struct TreeNode *root) {
    bool res = true;
    struct TreeNode *q1[1000] = {NULL}, *q2[1000] = {NULL};
    int front1 = -1, front2 = -1, rear1 = -1, rear2 = -1;
    if (root->left->val == root->right->val) {
        enqueue(q1, &rear1, &front1, root->left);   // корень в очередь
        enqueue(q2, &rear2, &front2, root->right);  // корень в очередь
        struct TreeNode *item1, *item2;
        while (rear1 >= front1 && rear2 >= front2 && rear1 == rear2) {
            item1 = dequeue(q1, &rear1, &front1);
            item2 = dequeue(q2, &rear2, &front2);
            // printf("vals:%d %d ", item1->val,item2->val);
            if (item1->val == item2->val && ((item1->right == NULL) == (item2->left == NULL)) &&
                ((item2->right == NULL) == (item1->left == NULL))) {
                if (item1->left) enqueue(q1, &rear1, &front1, item1->left);
                if (item1->right) enqueue(q1, &rear1, &front1, item1->right);
                if (item2->right) enqueue(q2, &rear2, &front2, item2->right);
                if (item2->left) enqueue(q2, &rear2, &front2, item2->left);
            } else {
                res = false;
                break;
            }
        }
    } else {
        res = false;
    }
    if (rear1 != rear2) res = false;
    return res;
}

void print_tree(struct TreeNode *root) {
    struct TreeNode *q1[1000];
    int front = -1, rear = -1;
    enqueue(q1, &rear, &front, root);  // корень в очередь
    struct TreeNode *item1;
    while (rear >= front) {
        item1 = dequeue(q1, &rear, &front);
        printf("%d ", item1->val);
        if (item1->left)  // левое поддерево
            enqueue(q1, &rear, &front, item1->left);
        if (item1->right)  //  правое поддерево
            enqueue(q1, &rear, &front, item1->right);
    }
    printf("\n");
}

struct TreeNode *tree_init(struct TreeNode **items, int itemssize) {
    struct TreeNode *root = malloc(sizeof(struct TreeNode));
    root->val = 1;

    for (int i = 0; i < itemssize; i++) {
        items[i] = malloc(sizeof(struct TreeNode));
        items[i]->val = (i + 4) / 2;
        items[i]->left = items[i]->right = NULL;
    }
    root->left = items[0];
    root->right = items[1];
    items[0]->left = items[2];
    items[0]->right = items[4];
    items[1]->left = items[5];
    items[1]->right = items[3];

    return root;
}

int main() {
    struct TreeNode *items[6];
    struct TreeNode *root = tree_init(items, 6);
    bool res = isSymmetric(root);

    print_tree(root);
    printf("Tree is %sSymmetric\n", ((res == true) ? "" : "not "));

    root->right->right->val = 5;
    print_tree(root);
    res = isSymmetric(root);
    printf("Tree is %sSymmetric\n", ((res == true) ? "" : "not "));

    root->left->left->val = 5;
    print_tree(root);
    res = isSymmetric(root);
    printf("Tree is %sSymmetric\n", ((res == true) ? "" : "not "));

    root->right->right = NULL;
    print_tree(root);
    res = isSymmetric_(root);
    printf("Tree is %sSymmetric\n", ((res == true) ? "" : "not "));

    root->left->left = NULL;
    print_tree(root);
    res = isSymmetric_(root);
    printf("Tree is %sSymmetric\n", ((res == true) ? "" : "not "));
}
