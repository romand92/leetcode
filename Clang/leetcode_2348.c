/*
Given an integer array nums, return the number of subarrays filled with 0.

A subarray is a contiguous non-empty sequence of elements within an array.
*/
#include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 3

long long sum(long long n) { return n * (n + 1) / 2; }

long long zeroFilledSubarray(int* nums, int numsSize) {
    long long res = 0;
    int n = 0;
    for (int i = 0; i < numsSize; i++) {
        if (nums[i] == 0)
            n++;
        else {
            if (n) {
                res += sum(n);
                n = 0;
            }
        }
    }
    return res + sum(n);
}

int main() {
    int test_arr[SIZEOFTESTARRAY][8] = {{1, 3, 0, 0, 2, 0, 0, 4}, {0, 0, 0, 2, 0, 0}, {2, 10, 2019}};
    int test_arr_lenght[] = {8, 6, 3};
    int expected_values[SIZEOFTESTARRAY] = {6, 9, 0};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int val = zeroFilledSubarray(test_arr[i], test_arr_lenght[i]);
        printf("%d expected:%d %s\n", val, expected_values[i],
               (val != expected_values[i]) ? "Fail" : "Success");
    }
}
