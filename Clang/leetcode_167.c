/*
167. Two Sum II - Input Array Is Sorted

Given a 1-indexed array of integers numbers that is already sorted in non-decreasing order, find two numbers
such that they add up to a specific target number. Let these two numbers be numbers[index1] and
numbers[index2] where 1 <= index1 < index2 <= numbers.length.

Return the indices of the two numbers, index1 and index2, added by one as an integer array [index1, index2] of
length 2.

The tests are generated such that there is exactly one solution. You may not use the same element twice.

Your solution must use only constant extra space.
*/

#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#define SIZEOFTESTARRAY 3

int* twoSum(int* numbers, int numbersSize, int target, int* returnSize) {
    int* res = malloc(sizeof(int) * 2);
    // *returnSize = 0;
    for (int i = 0; i < numbersSize - 1; i++) {
        if (i == 0 || numbers[i] != numbers[i - 1]) {
            int target_numbers = target - numbers[i];
            int min = i + 1;
            int max = numbersSize - 1;
            int mid;
            while (min < max) {
                mid = min + (max - min + 1) / 2;
                if (numbers[mid] <= target_numbers) {
                    min = mid;
                } else {
                    max = mid - 1;
                }
            }
            if (target_numbers == numbers[min]) {
                res[0] = i + 1;
                res[1] = min + 1;
                break;
            }
        }
    }
    *returnSize = 2;
    return res;
}

void print_arr(int* arr, int sz) {
    printf("[");
    for (int i = 0; i < sz; i++) printf("%d%s", arr[i], (i == sz - 1) ? "] " : ", ");
}

int main() {
    int test_arr[SIZEOFTESTARRAY][5] = {{2, 7, 11, 15}, {2, 3, 4}, {-1, 0}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {5, 3, 2};
    int test_var1[SIZEOFTESTARRAY] = {9, 6, -1};

    int expected_values[SIZEOFTESTARRAY][2] = {{1, 2}, {1, 3}, {1, 2}};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int res = 1;
        int val_sz = 0;
        int* val = twoSum(test_arr[i], test_arr_lenght[i], test_var1[i], &val_sz);

        print_arr(test_arr[i], test_arr_lenght[i]);
        printf(" target = %d. Result: ", test_var1[i]);
        print_arr(val, val_sz);
        for (int j = 0; j < val_sz; j++) {
            if (expected_values[i][j] != val[j]) res = 0;
        }

        printf(" %s\n", (res) ? "Success" : "Fail");
        free(val);
    }
}
