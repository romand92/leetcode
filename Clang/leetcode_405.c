/*
Given an integer num, return a string representing its hexadecimal
representation. For negative integers, two’s complement method is used.

All the letters in the answer string should be lowercase characters, and there
should not be any leading zeros in the answer except for the zero itself.

Note: You are not allowed to use any built-in library method to directly solve
this problem.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* toHex(int num) {
    char nums[] = "0123456789abcdef";
    int base = 16;
    unsigned int num_u = (unsigned int)num;
    // printf("%u", num);
    char* res = NULL;
    int i = 0;
    do {
        char a = nums[num_u % base];
        // printf("%c",a);
        res = realloc(res, sizeof(char) * (++i));
        res[i - 1] = a;
        num_u = num_u / base;
    } while (num_u > 0);  // in case of input '0'
    res = realloc(res, sizeof(char) * (i + 1));
    res[i] = '\0';
    char tmp;
    i--;
    for (int j = 0; j <= i; j++, i--) {
        if (res[i] != res[j]) {
            tmp = res[i];
            res[i] = res[j];
            res[j] = tmp;
        }
    }

    return res;
}

int main() {
    int test_arr[] = {32, -1, 0};
    int test_arr_lenght = 3;
    char* expected_valuse[] = {"20", "ffffffff", "0"};
    for (int i = 0; i < test_arr_lenght; i++) {
        char* val = toHex(test_arr[i]);
        printf("result:'%s' expected:'%s' %s\n", val, expected_valuse[i],
               (strcmp(val, expected_valuse[i]) != 0) ? "Fail" : "Success");
        free(val);
    }
}