/*
1768. Merge Strings Alternately
Easy

You are given two strings word1 and word2. Merge the strings by adding letters in alternating order, starting
with word1. If a string is longer than the other, append the additional letters onto the end of the merged
string.

Return the merged string.

1 <= word1.length, word2.length <= 100
word1 and word2 consist of lowercase English letters.
*/

#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#define SIZEOFTESTARRAY 3

char* mergeAlternately(char* word1, char* word2);

int main() {
    char test_arr1[SIZEOFTESTARRAY][5] = {"abc", "ab", "abcd"};
    char test_arr2[SIZEOFTESTARRAY][5] = {"pqr", "pqrs", "pq"};

    char* expected_values[SIZEOFTESTARRAY] = {"apbqcr", "apbqrs", "apbqcd"};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        char* val = mergeAlternately(test_arr1[i], test_arr2[i]);
        printf("Word1: \"%s\", word2: \"%s\", merged: \"%s\",\n\t\t\t  expected: \"%s\", result - %s\n",
               test_arr1[i], test_arr2[i], val, expected_values[i],
               (strcmp(val, expected_values[i]) == 0) ? "Success" : "Fail");
        free(val);
        // printf("%d expected:%d %s\n", val, expected_values[i],
        //        (val != expected_values[i]) ? "Fail" : "Success");
    }
}

char* mergeAlternately(char* word1, char* word2) {
    char* res = malloc((strlen(word1) + strlen(word2) + 1) * sizeof(char));
    char* res_start = res;
    int i = 0;
    while (*word1 && *word2) {
        // if (i%2 == 0){
        //     *res = *word1++;
        // }else{
        //     *res = *word2++;
        // }
        // res++;i++;
        *res = *word1++;
        *(++res) = *word2++;
        res++;
    }

    while (*word1) {
        *res = *word1++;
        res++;
    }
    while (*word2) {
        *res = *word2++;
        res++;
    }
    *res = '\0';
    return res_start;
}