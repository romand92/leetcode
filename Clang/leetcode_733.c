/*
733. Flood Fill
Easy

An image is represented by an m x n integer grid image where image[i][j] represents the pixel value of the
image.

You are also given three integers sr, sc, and color. You should perform a flood fill on the image starting
from the pixel image[sr][sc].

To perform a flood fill, consider the starting pixel, plus any pixels connected 4-directionally to the
starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those
pixels (also with the same color), and so on. Replace the color of all of the aforementioned pixels with
color.

Return the modified image after performing the flood fill.
*/

#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#define SIZEOFTESTARRAY 2

void floodFill_recoursive(int** image, int imageSize, int* imageColSize, int sr, int sc, int color,
                          int color_to_flood);
int** floodFill(int** image, int imageSize, int* imageColSize, int sr, int sc, int color, int* returnSize,
                int** returnColumnSizes);
// int ** arrayToPinter(int * Arr, int N, int M){
//     int *ptrs[N];
//     for(int i = 0, i < N, i++)

// }
void print_2D_arr(int** array, int N, int* M);

int main() {
    int test_arr[SIZEOFTESTARRAY][3][3] = {{{1, 1, 1}, {1, 1, 0}, {1, 0, 1}},
                                           {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}};
    // int *test_arrptr[2] = arrayToPinter(test_arr[0],3,3);
    int N = 3;
    int test_arr_lenght[3] = {3, 3, 3};  // int M = 3;
    int test_color[SIZEOFTESTARRAY] = {2, 0};

    int* test_arr_ptr[2][3];  // test_arr -> **ptr
    for (int k = 0; k < SIZEOFTESTARRAY; k++) {
        int* ptr = test_arr[k][0];
        for (int i = 0; i < N; i++) test_arr_ptr[k][i] = ptr + i * test_arr_lenght[0];
    }

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int res_N, *res_M;
        int** res;
        printf("Initial array:\n");
        print_2D_arr(test_arr_ptr[i], N, test_arr_lenght);
        res = floodFill(test_arr_ptr[i], N, test_arr_lenght, 1, 1, test_color[i], &res_N, &res_M);
        printf("Result array:\n");
        print_2D_arr(res, N, test_arr_lenght);
        free(res_M);
    }
}

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
void floodFill_recoursive(int** image, int imageSize, int* imageColSize, int sr, int sc, int color,
                          int color_to_flood) {
    //  printf("invoked %d %d\n", sr,sc);
    if ((sr >= 0 && sr < imageSize) && (sc >= 0 && sc < *imageColSize) && image[sr][sc] == color_to_flood) {
        // int color_before = image[sr][sc];
        image[sr][sc] = color;
        floodFill_recoursive(image, imageSize, imageColSize, sr - 1, sc, color, color_to_flood);
        floodFill_recoursive(image, imageSize, imageColSize, sr + 1, sc, color, color_to_flood);
        floodFill_recoursive(image, imageSize, imageColSize, sr, sc - 1, color, color_to_flood);
        floodFill_recoursive(image, imageSize, imageColSize, sr, sc + 1, color, color_to_flood);
    }
}

int** floodFill(int** image, int imageSize, int* imageColSize, int sr, int sc, int color, int* returnSize,
                int** returnColumnSizes) {
    *returnSize = imageSize;
    *returnColumnSizes = malloc(sizeof(int) * imageSize);
    // printf("%d %d\n", *returnSize,*imageColSize);
    for (int i = 0; i < imageSize; i++) {
        // printf("i:%d ",i);
        (*returnColumnSizes)[i] = imageColSize[i];
        // printf("%d ",*imageColSize);}
    }
    if (image[sr][sc] != color)
        floodFill_recoursive(image, imageSize, imageColSize, sr, sc, color, image[sr][sc]);

    return image;
}

void print_2D_arr(int** array, int N, int* M) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M[i]; j++) printf("%d ", array[i][j]);
        printf("\n");
    }
}