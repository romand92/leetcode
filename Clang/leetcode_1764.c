#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

void free_arrays(int **matches, int groupsSize, int *goup_iter) {
    for (int i = 0; i < groupsSize; i++) free(matches[i]);
    free(matches);
    free(goup_iter);
}

void print_matches(int **matches, int rows, int with_size) {
    for (int i = 0; i < rows; i++) {
        for (int j = ((with_size) ? 0 : 1); j < matches[i][0]; j++) printf("%d ", matches[i][j]);
        printf("\n");
    }
}
bool _canChoose(int **groups, int groupsSize, int *groupsColSize, int *nums, int numsSize) {
    //       groups count   len of each gr
    printf("%d %d\n", groupsSize, *groupsColSize);
    int *goup_iter = malloc(sizeof(int) * groupsSize);  // iterator for each group
    for (int i = 0; i < groupsSize; i++) goup_iter[i] = 0;
    int **matches = malloc(sizeof(int *) * groupsSize);  // 2d-array with all the matches of all grops
    for (int i = 0; i < groupsSize; i++) {
        matches[i] = malloc(sizeof(int) * 2);
        matches[i][0] = 2;  // row size
        matches[i][1] = 0;
    }

    int with_size = 0;
    print_matches(matches, groupsSize, with_size);
    free_arrays(matches, groupsSize, goup_iter);
    // for (int i=0;i<groupsSize;i++)
    //     free(matches[i]);
    // free(matches);
    // free(goup_iter);
    return false;

    for (int k = 0; k < numsSize; k++)                // 0...nums...k
        for (int i = 0; i < groupsSize; i++)          // 0...[group][group]...i
            for (int j = 0; j < *groupsColSize; j++)  // [0...group...j]
                if (groups[i][j + goup_iter[i]] == nums[k]) {
                    if (++goup_iter[i] == *groupsColSize - 1) {
                        // full match from k-groupsColSize to k
                        matches[i] = realloc(matches[i], sizeof(int) * matches[i][0]++);
                        // if (matches[i] == NULL) X_X
                        goup_iter[i] = 0;
                    }
                } else {
                    goup_iter[i] = 0;
                }
}

bool canChoose(int **groups, int groupsSize, int *groupsColSize, int *nums, int numsSize) {
    // printf("%d %d",groupsSize,groupsColSize[0]);
    int k = 0;  // 0...nums...k
    int i = 0;  // 0...[group][group]...i
    int j = 0;  // [0...group...j]
    while (k < numsSize && i < groupsSize) {
        // printf("n:%d g[%d][%d/%d]:%d; ",nums[k],i,j,groupsColSize[i],
        // groups[i][j]);
        if (nums[k++] == groups[i][j])  // find last digit might be faster
        {
            for (j = 1; j < groupsColSize[i] && k < numsSize; k++, j++)
                if (nums[k] != groups[i][j]) break;
            if (j == groupsColSize[i]) {
                // printf("<-match%d\t",i);
                i++;
            } else {
                k -= j - 1;
            }
            j = 0;
        }

        // k++;
    }

    return ((i == groupsSize) ? true : false);
}

int main() {
    int groups[2][3] = {{1, -1, -1}, {3, -2, 0}};
    int nums[] = {1, -1, 0, 1, -1, -1, 3, -2, 0};
    int groupcols[] = {3, 3};
    int rows = 2, cols = 3;
    int **x;

    x = malloc(sizeof(int *) * rows);
    int *ptr = (int *)(x + rows);
    for (int i = 0; i < rows; i++) x[i] = (groups[i]);

    printf("%s", ((canChoose(x, 2, groupcols, nums, sizeof(nums) / sizeof(int))) ? "True" : "False"));
}
