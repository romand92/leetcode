/*
189. Rotate Array
Medium

Given an integer array nums, rotate the array to the right by k steps, where k is non-negative.
*/

#include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 4

void rotate(int* nums, int numsSize, int k) {  // numsSize>=1
    k = k % numsSize;
    // for(int i = 0; i<k; i++)//k steps, each 1 rotate by 1 pos
    // {
    //     int tmp = nums[numsSize - 1];
    //     for(int j = numsSize - 2; j>=0; j--) //shift for one position per iteration
    //         nums[j+1] = nums[j];
    //     nums[0] = tmp;

    //     // for (int j = 0; j <numsSize; j++){
    //     //     printf("%d ", nums[j]);
    //     //printf("\n");
    // }
    if (k > 0) {
        if (numsSize % k != 0 || k == 1) {
            int tmp = nums[k];
            int idx_where = k;
            for (int i = 0; i > -k * (numsSize - 1); i -= k) {
                int idx_from = (((i) % numsSize) + numsSize) % numsSize;
                nums[idx_where] = nums[idx_from];
                idx_where = idx_from;
            }
            nums[(2 * k) % numsSize] = tmp;
        } else {
            rotate(nums, numsSize, k - 1);
            rotate(nums, numsSize, 1);
        }
    }
}

void print_arr(int* arr, int arr_sz) {
    printf("[");
    for (int i = 0; i < arr_sz - 1; i++) printf("%d, ", arr[i]);
    printf("%d] ", arr[arr_sz - 1]);
}

int main() {
    int test_arr[SIZEOFTESTARRAY][7] = {{1, 2, 3, 4, 5, 6, 7}, {-1, -100, 3, 99}, {-1, -100, 3, 99}, {17}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {7, 4, 4, 1};
    int test_shift[SIZEOFTESTARRAY] = {3, 2, 55, 5};

    int expected_values[SIZEOFTESTARRAY][7] = {
        {5, 6, 7, 1, 2, 3, 4}, {3, 99, -1, -100}, {-100, 3, 99, -1}, {17}};

    // for array
    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int res = 1;
        printf("Array: ");
        print_arr(test_arr[i], test_arr_lenght[i]);
        rotate(test_arr[i], test_arr_lenght[i], test_shift[i]);
        printf("Result: ");
        print_arr(test_arr[i], test_arr_lenght[i]);
        for (int j = 0; j < test_arr_lenght[i]; j++) {
            if (expected_values[i][j] != test_arr[i][j]) res = 0;
        }

        printf(" %s\n", (res) ? "Success" : "Fail");
    }
}
