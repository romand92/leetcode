/*
Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result
must appear as many times as it shows in both arrays and you may return the result in any order.
*/
#include <stdio.h>
#include <stdlib.h>

#define SIZEOFTESTARRAY 4

int compare(const void* a1, const void* a2) { return *(int*)a1 - *(int*)a2; }

int* intersect(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize) {
    if (nums1Size > nums2Size) {
        int* tmp = nums1;
        nums1 = nums2;
        nums2 = tmp;
        int tmp2 = nums2Size;
        nums2Size = nums1Size;
        nums1Size = tmp2;
    }
    qsort(nums1, nums1Size, sizeof(int), compare);
    qsort(nums2, nums2Size, sizeof(int), compare);

    *returnSize = 0;
    if (nums1[0] > nums2[nums2Size - 1] || nums2[0] > nums1[nums1Size - 1]) return NULL;

    int* res = malloc(sizeof(int) * nums1Size);
    for (int j = 0, i = 0; j < nums2Size && i < nums1Size; j++) {
        while (nums1[i] < nums2[j]) {
            if (i < nums1Size - 1)
                i++;
            else
                break;
        }

        if (nums1[i] == nums2[j]) {
            res[(*returnSize)++] = nums1[i++];
        }
    }
    res = realloc(res, sizeof(int) * *returnSize);
    return res;
}

int main() {
    int test_arr[][5] = {{1, 2, 2, 1}, {2, 2}, /*   */ {4, 9, 5}, {9, 4, 9, 8, 4}};
    int test_arr_lenght[] = {4, 2, 3, 5};
    // int test_var1[] = {};
    int expected_values[2][2] = {{2, 2}, /*  */ {4, 9}};
    int expected_values_col = 2;

    for (int i = 0; i < SIZEOFTESTARRAY; i += 2) {
        int valsize = 0;
        int* val =
            intersect(test_arr[i], test_arr_lenght[i], test_arr[i + 1], test_arr_lenght[i + 1], &valsize);
        int correct = 1;

        printf("[");
        for (int j = 0; j < valsize; j++) {
            printf("%d%s", val[j], ((j == SIZEOFTESTARRAY / 2 - 1) ? "] " : ", "));
            if (val[j] != expected_values[i / 2][j]) correct = 0;
        }
        printf("expected: [");
        for (int j = 0; j < SIZEOFTESTARRAY / 2; j++) {  // SIZEOFTESTARRAY != expected_cols
            printf("%d%s", expected_values[i / 2][j], ((j == SIZEOFTESTARRAY / 2 - 1) ? "] " : ", "));
        }
        printf("%s\n", (correct) ? "Success" : "Fail");
    }
}
