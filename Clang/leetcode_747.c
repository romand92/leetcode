// You are given an integer array nums where the largest integer is unique.
// Determine whether the largest element in the array is at least twice as much
// as every other number in the array. If it is, return the index of the largest
// element, or return -1 otherwise.
#include <stdio.h>

int dominantIndex(int* nums, int numsSize) {
    int max_i = 0;
    int second_max = 0;
    for (int i = 1; i < numsSize; i++)
        if (nums[i] > nums[max_i]) {
            second_max = nums[max_i];
            max_i = i;
        } else if (second_max < nums[i]) {
            second_max = nums[i];
        }

    return ((nums[max_i] >= 2 * second_max) ? max_i : -1);
}
int main() {
    int test[5][4] = {{3, 6, 1, 0}, {1, 2, 3, 4}, {0, 0, 0, 1}, {0, 0, 3, 2}, {1, 0}};
    for (int i = 0; i < 5; i++) {
        printf("%d\n", dominantIndex(test[i], (i == 4) ? 2 : 4));
    }
}