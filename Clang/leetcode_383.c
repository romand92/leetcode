/*
Given two strings ransomNote and magazine, return true if ransomNote can be constructed by using the letters
from magazine and false otherwise.

Each letter in magazine can only be used once in ransomNote.
*/
#include <stdbool.h>
#include <stdio.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 3

#define base 26
bool canConstruct(char* ransomNote, char* magazine) {
    // base = 26;
    int res = true;
    int hash_raansom[base] = {0}, hash_mag[base] = {0};
    for (; *ransomNote; ransomNote++) hash_raansom[*ransomNote - 'a']++;
    for (; *magazine; magazine++) hash_mag[*magazine - 'a']++;
    for (int i = 0; i < base; i++)
        if (hash_mag[i] < hash_raansom[i]) {
            // printf("%d %c",i, (i+'a'));
            return false;
        }
    return true;
}

int main() {
    char test_ransomNote[SIZEOFTESTARRAY][4] = {"a", "aa", "aa"};
    char test_magazine[SIZEOFTESTARRAY][4] = {"b", "ab", "aab"};
    int expected_values[SIZEOFTESTARRAY] = {false, false, true};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        bool val = canConstruct(test_ransomNote[i], test_magazine[i]);
        printf("\"%s\", \"%s\" - expected:%s %s\n", test_ransomNote[i], test_magazine[i],
               ((expected_values[i] == true) ? "true" : "false"),
               (val != expected_values[i]) ? "Fail" : "Success");
    }
}
