/*
You are given two integer arrays nums1 and nums2, sorted in non-decreasing order, and two integers m and n,
representing the number of elements in nums1 and nums2 respectively.

Merge nums1 and nums2 into a single array sorted in non-decreasing order.

The final sorted array should not be returned by the function, but instead be stored inside the array nums1.
To accommodate this, nums1 has a length of m + n, where the first m elements denote the elements that should
be merged, and the last n elements are set to 0 and should be ignored. nums2 has a length of n.
*/
#include <stdio.h>
#include <stdlib.h>

void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
    int j = m - 1;  // nums1
    int i = n - 1;  // nums2
    int res_i = m + n - 1;
    while (i >= 0) {
        if (j < 0 || nums2[i] > nums1[j])
            nums1[res_i--] = nums2[i--];
        else
            nums1[res_i--] = nums1[j--];
        // printf("[%d,%d]%d",i,j,nums1[res_i+1]);
    }
}

int compare(const void* a1, const void* a2) { return *(int*)a1 - *(int*)a2; }

void merge_qsort(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
    for (int i = 0; i < n; i++) nums1[m + i] = nums2[i];
    qsort(nums1, m + n, sizeof(int), compare);
}

int main() {
    int test_arr1[3][6] = {{1, 2, 3, 0, 0, 0}, {1}, {999}};  // 999 is ignored since arr_lenght is 0
    int test_arr2[3][3] = {{2, 5, 6}, {999}, {1}};
    int test_arr_1_lenght[] = {3, 1, 0};
    int test_arr_2_lenght[] = {3, 0, 1};
    int expected_values[][6] = {{1, 2, 2, 3, 5, 6}, {1}, {1}};

    for (int i = 0; i < 3; i++) {
        merge(test_arr1[i], test_arr_1_lenght[i], test_arr_1_lenght[i], test_arr2[i], test_arr_2_lenght[i],
              test_arr_2_lenght[i]);
        int is_correct = 1;
        printf("[ ");
        for (int j = 0; j < test_arr_1_lenght[i] + test_arr_2_lenght[i]; j++) {
            printf("%d ", test_arr1[i][j]);
            if (test_arr1[i][j] == expected_values[i][j]) {
                is_correct = 0;
            }
        }
        printf("] expected: [ ");
        for (int j = 0; j < test_arr_1_lenght[i] + test_arr_2_lenght[i]; j++)
            printf("%d ", expected_values[i][j]);
        printf("] %s", (is_correct) ? "Fail\n" : "Success\n");
    }
}