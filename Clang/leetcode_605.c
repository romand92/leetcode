/*
You have a long flowerbed in which some of the plots are planted, and some are not. However, flowers cannot be
planted in adjacent plots.

Given an integer array flowerbed containing 0's and 1's, where 0 means empty and 1 means not empty, and an
integer n, return if n new flowers can be planted in the flowerbed without violating the no-adjacent-flowers
rule.
*/
#include <stdbool.h>
#include <stdio.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 3

bool canPlaceFlowers(int* flowerbed, int flowerbedSize, int n) {
    for (int i = 0; i < flowerbedSize && n; i++) {
        if ((i < flowerbedSize - 1) && flowerbed[i + 1]) {
            i += 2;
            continue;
        }  //+else if -continue slower
        if (flowerbed[i]) {
            i++;
            continue;
        }
        if (i == 0 || !flowerbed[i - 1]) {
            n--;
            i++;
        }
    }
    return !n;
}

int main() {
    int test_arr[SIZEOFTESTARRAY][5] = {{1, 0, 0, 0, 1}, {1, 0, 0, 0, 1}, {0, 0, 1}};
    int test_arr_lenght[] = {5, 5, 3};
    int test_arr_n[] = {1, 2, 1};

    int expected_values[SIZEOFTESTARRAY] = {true, false, true};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        bool val = canPlaceFlowers(test_arr[i], test_arr_lenght[i], test_arr_n[i]);
        printf("%d expected:%d %s\n", val, expected_values[i],
               (val != expected_values[i]) ? "Fail" : "Success");
    }
}
