/*
You are given an m x n integer matrix matrix with the following two properties:
    Each row is sorted in non-decreasing order.
    The first integer of each row is greater than the last integer of the previous row.
Given an integer target, return true if target is in matrix or false otherwise.

You must write a solution in O(log(m * n)) time complexity.
*/
#include <stdbool.h>
#include <stdio.h>
#define SIZEOFTESTARRAY 3

bool searchMatrix(int** matrix, int matrixSize, int* matrixColSize, int target) {
    int min_index = 0;
    int max_index = matrixSize * matrixColSize[0] - 1;  // m*n all rows are filled
    int mid_index, value;
    while (min_index < max_index) {
        mid_index = (min_index + max_index) / 2;
        value = matrix[mid_index / matrixColSize[0]][mid_index % matrixColSize[0]];
        // printf ("%d:%d ",mid_index,value);
        if (value > target) {
            max_index = mid_index - 1;
        } else if (value < target) {
            min_index = mid_index + 1;
        } else {
            min_index = mid_index;
            break;
        }
    }
    // printf ("\n%d:%d %d %d ",mid_index,value,min_index,max_index);
    if (matrix[min_index / matrixColSize[0]][min_index % matrixColSize[0]] == target)
        return true;
    else
        return false;
}

int main() {
    int test_arr[SIZEOFTESTARRAY][4] = {{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}};
    int* test_arr_ptr[SIZEOFTESTARRAY];
    int* ptr = test_arr[0];
    for (int i = 0; i < SIZEOFTESTARRAY; i++) test_arr_ptr[i] = ptr + i * 4;
    int test_arr_lenght[3] = {4, 4, 4};
    int expected_values[] = {true, false};
    int sizeOfTestArr = sizeof(test_arr) / sizeof(test_arr[0]);

    bool val = searchMatrix(test_arr_ptr, 3, test_arr_lenght, 3);
    printf("%d expected:%d %s\n", val, expected_values[0], (val != expected_values[0]) ? "Fail" : "Success");

    val = searchMatrix(test_arr_ptr, 3, test_arr_lenght, 14);
    printf("%d expected:%d %s\n", val, expected_values[1], (val != expected_values[1]) ? "Fail" : "Success");
}
