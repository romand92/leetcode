/*
695. Max Area of Island
Medium
You are given an m x n binary matrix grid. An island is a group of 1's (representing land) connected
4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

The area of an island is the number of cells with a value 1 in the island.

Return the maximum area of an island in grid. If there is no island, return 0.
*/

// #include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>

int connected_grounds(int** grid, int gridSize, int* gridColSize, int x, int y) {
    int res = 0;
    if (x >= 0 && x < gridSize && y >= 0 && y < gridColSize[x]) {
        res = grid[x][y];
        grid[x][y] = 0;
        if (res) {
            res += connected_grounds(grid, gridSize, gridColSize, x - 1, y) +
                   connected_grounds(grid, gridSize, gridColSize, x + 1, y) +
                   connected_grounds(grid, gridSize, gridColSize, x, y - 1) +
                   connected_grounds(grid, gridSize, gridColSize, x, y + 1);
        }
    }
    return res;
}

int maxAreaOfIsland(int** grid, int gridSize, int* gridColSize) {
    int max_island = 0;
    for (int i = 0; i < gridSize; i++)
        for (int j = 0; j < gridColSize[i]; j++) {
            if (grid[i][j]) {
                int island = connected_grounds(grid, gridSize, gridColSize, i, j);
                if (island > max_island) max_island = island;
            }
        }
    return max_island;
}