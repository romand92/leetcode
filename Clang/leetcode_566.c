/*
In MATLAB, there is a handy function called reshape which can reshape an m x n matrix into a new one with a
different size r x c keeping its original data.

You are given an m x n matrix mat and two integers r and c representing the number of rows and the number of
columns of the wanted reshaped matrix.

The reshaped matrix should be filled with all the elements of the original matrix in the same row-traversing
order as they were.

If the reshape operation with given parameters is possible and legal, output the new reshaped matrix;
Otherwise, output the original matrix.
*/
#include <stdio.h>
#include <stdlib.h>

//#define SIZEOFTESTARRAY 4

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
int** matrixReshape(int** mat, int matSize, int* matColSize, int r, int c, int* returnSize,
                    int** returnColumnSizes) {
    int** res = NULL;
    int size = 0;
    for (int i = 0; i < matSize; i++) size += matColSize[i];
    if (size > 0 && size == r * c) {
        res = malloc(sizeof(int) * r * c + sizeof(int*) * r);
        int* colsz = malloc(sizeof(int) * r);
        int* ptr = (int*)(res + r);
        for (int i = 0; i < r; i++) {
            res[i] = ptr + i * c;
            colsz[i] = c;
        }
        int index = 0;
        while (index < r * c) {
            res[index / c][index % c] = mat[index / (*matColSize)][index % *matColSize];
            index++;
        }
        *returnSize = r;
        *returnColumnSizes = colsz;

    } else {
        res = mat;
        *returnSize = matSize;
        *returnColumnSizes = matColSize;
    }
    return res;
}

void print_matr(int** matr, int rows, int* cols) {
    printf("\n matr [%d,%d]:\n", rows, cols[0]);

    // printf("returnSize: \n",rows);
    //     for(int i = 0; i < rows; i++)
    //         printf("col-%d size:%d\n",i,cols[i]);
    for (int j = 0; j < rows; j++) {
        for (int i = 0; i < cols[j]; i++) printf("%d ", matr[j][i]);
        printf("\n");
    }
}

int main() {
    int test_arr[2][2] = {{1, 2}, {3, 4}};
    int* test_mat[2] = {test_arr[0], test_arr[1]};
    int test_mat_cols_sz[2] = {2, 2};
    int val_ret_sz;
    int* val_ret_col_sz;

    int** val = matrixReshape(test_mat, 2, test_mat_cols_sz, 1, 4, &val_ret_sz, &val_ret_col_sz);
    print_matr(val, val_ret_sz, val_ret_col_sz);
    free(val);
    free(val_ret_col_sz);
    val = matrixReshape(test_mat, 2, test_mat_cols_sz, 4, 1, &val_ret_sz, &val_ret_col_sz);
    print_matr(val, val_ret_sz, val_ret_col_sz);
    free(val);
    free(val_ret_col_sz);
    val = matrixReshape(test_mat, 2, test_mat_cols_sz, 2, 10, &val_ret_sz, &val_ret_col_sz);
    print_matr(val, val_ret_sz, val_ret_col_sz);
    free(val);
    free(val_ret_col_sz);
}
