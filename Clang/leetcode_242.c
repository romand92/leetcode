/*
Given two strings s and t, return true if s can be constructed by using the letters
from t and false otherwise.

Each letter in t can only be used once in s.
*/
#include <stdbool.h>
#include <stdio.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 3

#define base 26
bool isAnagram(char* s, char* t) {
    // base = 26;
    int res = true;
    int hash[base] = {0};
    for (; *s; s++) hash[*s - 'a']++;
    for (; *t; t++) hash[*t - 'a']--;
    for (int i = 0; i < base; i++)
        if (hash[i]) {
            // printf("%d %c",i, (i+'a'));
            return false;
        }
    return true;
}

int main() {
    char test_s[SIZEOFTESTARRAY][10] = {"anagram", "car", "aba"};
    char test_t[SIZEOFTESTARRAY][10] = {"nagaram", "rat", "aab"};
    int expected_values[SIZEOFTESTARRAY] = {true, false, true};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        bool val = isAnagram(test_s[i], test_t[i]);
        printf("\"%s\", \"%s\" - expected:%s %s\n", test_s[i], test_t[i],
               ((expected_values[i] == true) ? "true" : "false"),
               (val != expected_values[i]) ? "Fail" : "Success");
    }
}
