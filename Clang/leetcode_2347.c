/*
You are given an integer array ranks and a character array suits. You have 5 cards where the ith card has a
rank of ranks[i] and a suit of suits[i].

The following are the types of poker hands you can make from best to worst:

"Flush": Five cards of the same suit.
"Three of a Kind": Three cards of the same rank.
"Pair": Two cards of the same rank.
"High Card": Any single card.
Return a string representing the best type of poker hand you can make with the given cards.

Note that the return values are case-sensitive.
*/
#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#define SIZEOFTESTARRAY 4

int compare(const void *a, const void *b) { return *(int *)a - *(int *)b; }

char *bestHand(int *ranks, int ranksSize, char *suits, int suitsSize) {
    int flush = 1, sameCards = 1, straight = 0;
    // is_flush
    for (int i = 1; i < suitsSize; i++) {
        if (suits[i] != suits[0]) {
            flush = 0;
            break;
        }
    }

    qsort(ranks, ranksSize, sizeof(int), compare);
    for (int i = 0; i < ranksSize - 1; i++) {
        int tmp = 1;
        for (int j = i + 1; j < ranksSize; j++) {
            if (ranks[i] == ranks[j]) tmp++;
            if (tmp > sameCards) sameCards = tmp;
        }
    }
    if ((sameCards == 1) && (ranks[4] - ranks[0] == 4)) straight = 1;

    if ((flush) && (straight)) return "Straight flush";
    if (flush) return "Flush";
    if (straight) return "Straight";
    if (sameCards == 4) return "Quads";
    if (sameCards == 3) return "Three of a Kind";
    if (sameCards == 2) return "Pair";
    return "High Card";
}

int main() {
    int test_arr_ranks[SIZEOFTESTARRAY][5] = {
        {13, 2, 3, 1, 9}, {4, 4, 2, 4, 4}, {10, 10, 2, 12, 9}, {11, 10, 14, 13, 12}};
    char test_arr_suits[SIZEOFTESTARRAY][5] = {{'a', 'a', 'a', 'a', 'a'},
                                               {'d', 'a', 'a', 'b', 'c'},
                                               {'a', 'b', 'c', 'a', 'd'},
                                               {'a', 'a', 'a', 'a', 'a'}};
    int test_arr_lenght[] = {5, 5, 5, 5};

    char expected_values[SIZEOFTESTARRAY][15] = {"Flush", "Quads", "Pair", "Straight flush"};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        char *val = bestHand(test_arr_ranks[i], test_arr_lenght[i], test_arr_suits[i], test_arr_lenght[i]);
        printf("%s expected:%s %s\n", val, expected_values[i],
               (strcmp(val, expected_values[i]) == 0) ? "Success" : "Fail");
    }
}
