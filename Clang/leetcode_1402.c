/*
1402. Reducing Dishes
Hard

A chef has collected data on the satisfaction level of his n dishes. Chef can cook any dish in 1 unit of time.

Like-time coefficient of a dish is defined as the time taken to cook that dish including previous dishes
multiplied by its satisfaction level i.e. time[i] * satisfaction[i].

Return the maximum sum of like-time coefficient that the chef can obtain after dishes preparation.

Dishes can be prepared in any order and the chef can discard some dishes to get this maximum value.
*/
#include <stdio.h>
// #include <stdbool.h>
#include <stdlib.h>
#define SIZEOFTESTARRAY 3

int comp(void const *a, void const *b) { return *(int *)b - *(int *)a; }
int maxSatisfaction(int *satisfaction, int satisfactionSize) {
    qsort(satisfaction, satisfactionSize, sizeof(int), comp);

    int max_sat = 0;
    for (int i = 1; i <= satisfactionSize; i++) {
        int sat = 0;
        int multi = i;
        for (int j = 0; j < i; j++) {
            sat += multi * satisfaction[j];  // multi = i-j
            multi--;
        }
        if (max_sat < sat)
            max_sat = sat;
        else
            break;  //
    }
    return max_sat;
}

int main() {
    int test_arr[SIZEOFTESTARRAY][5] = {{-1, -8, 0, 5, -9}, {4, 3, 2}, {-1, -4, -5}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {5, 3, 3};
    int expected_values[SIZEOFTESTARRAY] = {14, 20, 0};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int val = maxSatisfaction(test_arr[i], test_arr_lenght[i]);
        printf("%d expected:%d %s\n", val, expected_values[i],
               (val != expected_values[i]) ? "Fail" : "Success");
    }
}
