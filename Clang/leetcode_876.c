/*
876. Middle of the Linked List

Given the head of a singly linked list, return the middle node of the linked list.

If there are two middle nodes, return the second middle node.
*/

#include <stdio.h>

#include "ListNode.c"
// #include <stdbool.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 2

struct ListNode* middleNode(struct ListNode* head) {
    int n = 0;
    struct ListNode* h = head;
    while (h) {
        h = h->next;
        n++;
    }
    n = n / 2;
    h = head;
    while (n) {
        h = h->next;
        n--;
    }
    return h;
}

int main() {
    int test_arr[SIZEOFTESTARRAY][6] = {{1, 2, 3, 4, 5}, {1, 2, 3, 4, 5, 6}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {5, 6};
    // int test_var1[SIZEOFTESTARRAY] = {};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        struct ListNode* list = init_list_from_arr(test_arr[i], test_arr_lenght[i]);
        struct ListNode* mid = middleNode(list);
        printf("List:\n");
        print_list(list);
        printf("From mid:\n");
        print_list(mid);
        destroy_list(list);
        printf("\n");
    }
}
