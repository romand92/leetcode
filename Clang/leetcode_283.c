/*
283. Move Zeroes

Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the
non-zero elements.

Note that you must do this in-place without making a copy of the array.
*/

#include <stdio.h>
// #include <stdbool.h>
// #include <stdlib.h>
#define SIZEOFTESTARRAY 2
void swap_elements(int* i, int* j) {
    int tmp = *i;
    *i = *j;
    *j = tmp;
}

void moveZeroes(int* nums, int numsSize) {
    int zero_ammount = 0;

    for (int i = 0; i < numsSize; i++) {
        if (nums[i] == 0) {
            zero_ammount++;
        } else {
            swap_elements(&nums[i], &nums[i - zero_ammount]);
        }
    }
}

int main() {
    int test_arr[SIZEOFTESTARRAY][5] = {{0, 1, 0, 3, 12}, {0}};
    int test_arr_lenght[SIZEOFTESTARRAY] = {5, 1};

    int expected_values[SIZEOFTESTARRAY][5] = {{1, 3, 12, 0, 0}, {0}};

    for (int i = 0; i < SIZEOFTESTARRAY; i++) {
        int res = 1;
        moveZeroes(test_arr[i], test_arr_lenght[i]);
        printf("Result: ");
        for (int j = 0; j < test_arr_lenght[i]; j++) {
            if (expected_values[i][j] != test_arr[i][j]) res = 0;
            printf("%d ", test_arr[i][j]);
        }

        printf(" %s\n", (res) ? "Success" : "Fail");
    }
}
