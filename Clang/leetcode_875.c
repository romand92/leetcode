/*
Koko loves to eat bananas. There are n piles of bananas, the ith pile has
piles[i] bananas. The guards have gone and will come back in h hours.

Koko can decide her bananas-per-hour eating speed of k. Each hour, she chooses
some pile of bananas and eats k bananas from that pile. If the pile has less
than k bananas, she eats all of them instead and will not eat any more bananas
during this hour.

Koko likes to eat slowly but still wants to finish eating all the bananas before
the guards return.

Return the minimum integer k such that she can eat all the bananas within h
hours.
*/

#include <stdio.h>
//{3,6,7,11}
int calc_time(int* piles, int pilesSize, int speed) {
    int time_to_eat = 0;
    for (int i = 0; i < pilesSize; i++) {
        time_to_eat += (piles[i] + (speed - 1)) / speed;
    }
    return time_to_eat;
}

int minEatingSpeed(int* piles, int pilesSize, int h) {
    int min_speed = 1;
    int max_speed = 2147483647;
    int mid_speed;
    while (min_speed != max_speed) {
        mid_speed = min_speed + (max_speed - min_speed) / 2;  // in case of overflow
        int time_to_eat = calc_time(piles, pilesSize, mid_speed);
        if (time_to_eat <= h)
            max_speed = mid_speed;
        else
            min_speed = mid_speed + 1;
    }
    return min_speed;
}

int main() {
    int test_arr[5][5] = {{3, 6, 7, 11}, {30, 11, 23, 4, 20}, {30, 11, 23, 4, 20}, {0, 0, 3, 2}, {1, 0}};
    int test_arr_lenght[] = {4, 5, 5, 4, 2};
    int test_h[] = {8, 5, 6};
    int expected_valuse[] = {4, 30, 23};

    for (int i = 0; i < 3; i++) {
        int val = minEatingSpeed(test_arr[i], test_arr_lenght[i], test_h[i]);
        printf("%d expected:%d %s\n", val, expected_valuse[i],
               (val != expected_valuse[i]) ? "Fail" : "Success");
    }

    int arr[] = {
        960397057, 363273754, 865412576, 436082599, 3417229,   277945501, 763249011, 47331620,  355627167,
        695584191, 982721323, 511523587, 225187532, 84601345,  64984101,  692777402, 993988996, 688549190,
        84499026,  896288698, 711562692, 577774086, 569179226, 233618567, 157095061, 112350994, 449765262,
        897190854, 32618223,  916798693, 20152121,  745516806, 44284437,  838508803, 35230883,  522122680,
        434006144, 208739628, 630558596, 95883913,  677977308, 113497918, 146976123, 51580361,  632540602,
        137634914, 169185543, 875959011, 323795825, 176839645, 158643180, 250162027, 336591354, 740496602,
        908945251, 805754215, 39646521,  847227748, 730359228, 763527820, 959183098, 63118412,  388271349,
        370510708, 928907806, 419841124, 105226392, 377060021, 449786621, 17865089,  371069806, 539971485,
        155931689, 530480029, 50833798,  358526720, 131666859, 294488759, 520662602, 762550083, 445323712};
    int arr_l = sizeof(arr) / sizeof(int);
    int h = 27196366;
    int val = minEatingSpeed(arr, arr_l, h);
    int expect = 1300;
    printf("%d expected:%d %s\n", val, expect, (val != expect) ? "Fail" : "Success");
}
