/*
You are given an integer array nums and an array queries where queries[i] =
[vali, indexi].

For each query i, first, apply nums[indexi] = nums[indexi] + vali, then print
the sum of the even values of nums.

Return an integer array answer where answer[i] is the answer to the ith query.
*/
#include <stdio.h>
#include <stdlib.h>
int* sumEvenAfterQueries(int* nums, int numsSize, int** queries, int queriesSize, int* queriesColSize,
                         int* returnSize) {
    int* res = malloc(sizeof(int) * queriesSize);
    *returnSize = 0;
    int nums_sum = 0;
    for (int j = 0; j < numsSize; j++) nums_sum += ((nums[j] % 2 == 0) ? nums[j] : 0);

    for (int i = 0; i < queriesSize; i++) {
        if (queries[i][1] < numsSize) {
            if (queries[i][0]) {
                if (nums[queries[i][1]] % 2 == 0) nums_sum -= nums[queries[i][1]];
                nums[queries[i][1]] += queries[i][0];
                if (nums[queries[i][1]] % 2 == 0) nums_sum += nums[queries[i][1]];
            }
        }
        //  res = realloc(res, ++res_i * sizeof(int));
        res[(*returnSize)++] = nums_sum;
    }
    //*returnSize = res_i;
    return res;
}

int main() {
    // int test_arr[5][5] = {{3, 6, 7, 11}, {30, 11, 23, 4, 20}, {30, 11, 23, 4,
    // 20}, {0, 0, 3, 2}, {1, 0}}; int test_arr_lenght[] = {4, 5, 5, 4, 2}; int
    // test_h[] = {8, 5, 6};
    //
    int test_nums[] = {1, 2, 3, 4}, test_queries[][2] = {{1, 0}, {-3, 1}, {-4, 0}, {2, 3}},
        test_querie_colSize[] = {2, 2, 2, 2};
    int* test_q_ptr[4];  // <---vvv point static array as dynamic int **
    for (int i = 0; i < 4; i++) test_q_ptr[i] = test_queries[i];

    int val_size;
    int expected_valuse[] = {8, 6, 2, 4};

    int* val = sumEvenAfterQueries(test_nums, 4, test_q_ptr, 4, test_querie_colSize, &val_size);
    int val_correct = 1;
    printf("Result: [ ");
    for (int i = 0; i < val_size; i++) {
        if (val[i] != expected_valuse[i]) val_correct = 0;
        printf("%d%s", val[i], ((i != val_size - 1) ? ", " : ""));
    }
    printf(" ] %s ", ((val_correct) ? "Sucsess" : "Fail"));
}